using System;
using UnityEngine;
using System.Collections;
using System.Diagnostics;
using EAP;
using Debug = UnityEngine.Debug;

[ExecuteInEditMode]
public class ExternalAudioPluginComponent : MonoBehaviour
{
    [SerializeField]
    public string pluginCommand;

    [SerializeField]
    public bool showEditorDefault;

    [SerializeField]
    public bool instantiateOnRun;

    [SerializeField]
    public double cpu;

    [SerializeField]
    public byte[] serializedPlugin;

    [SerializeField]
    public float volume;

    [SerializeField]
    public float mix;

    [UniqueIdentifier]
    public string UniqueId;

    [SerializeField]
    public bool globalState;

    public string CurrentName
	{
		get
		{
			if (PluginIsValid())
			{
				return plugin.pluginInstance.Name;
			}
			else
			{
				return "";
			}
		}
	}

	private class InternalBridgePlugin : MonoBehaviour
	{

		void Start()
		{
			AudioSettings.OnAudioConfigurationChanged += AudioSettingsChanged;
		}

		private void AudioSettingsChanged(bool what)
		{
			if (pluginInstance != null)
			{
				lock (pluginInstance.ProcessorLock)
				{
					pluginInstance.Suspend();
					int buffers = 0; int samples = 0;
					AudioSettings.GetDSPBufferSize(out samples, out buffers);
					currentSampleRate = AudioSettings.outputSampleRate;
					pluginInstance.ResetIO(buffers / 2, buffers / 2, samples, currentSampleRate);
					pluginInstance.Resume();
				}
			}
		}

		public void InstantiatePlugin(Database.ID identifier)
		{
			DestroyInstance();
			if (!pluginInstance.LoadPlugin(identifier))
				throw new ArgumentException("EAP.Component.InternalInstance: " + identifier.Name + " couldn't be loaded.");

			if (AudioSettings.outputSampleRate > 0)
				AudioSettingsChanged(false);
		}

        public void DoActionSuspended(System.Action action)
        {
            lock(pluginInstance.ProcessorLock)
            {
                bool revert = false;
                if (!pluginInstance.Suspended)
                    revert = pluginInstance.Suspend();

                action();

                if (revert)
                    pluginInstance.Resume();
            }
        }

		void DestroyInstance()
		{
			if (pluginInstance != null)
			{
                if (Settings.Verbose)
                    Debug.Log("EAP.Component.InternalInstance: Destroying Object that is valid? " + pluginInstance.HasValidPlugin());
				pluginInstance.Dispose();
			}
		}

		void OnDestroy()
		{
			DestroyInstance();
		}

		public double currentSampleRate;
		public EAP.Instance pluginInstance = new Instance();
	}

	private InternalBridgePlugin plugin;
	private GameObject pluginContainer;
	private double cpuUsage;
	private Stopwatch watch = new Stopwatch();
	private long frameCounter;
	// Use this for initialization


	public bool InstantiatePlugin(string newPluginIdentifier)
	{
		return InstantiatePlugin(Database.Instance().GetMatchingPluginIDFromName(pluginCommand));
	}

	public bool InstantiatePlugin(Database.ID newPluginIdentifier)
	{
		if (!newPluginIdentifier.IsValid)
		{
			print("EAP.Component: Invalid plugin ID trying to be instantiated: " + newPluginIdentifier);
			return false;
		}
		if (GetComponent<AudioSource>() == null && GetComponent<AudioListener>() == null)
			gameObject.AddComponent<AudioSource>();

		InternalBridgePlugin prePlugin = null;

		if (pluginContainer == null)
		{
			pluginContainer = new GameObject("");
			pluginContainer.GetComponent<Transform>().parent = GetComponent<Transform>();

		}

		prePlugin = pluginContainer.GetComponent<InternalBridgePlugin>();
		if(prePlugin == null)
			prePlugin = pluginContainer.AddComponent<InternalBridgePlugin>();

		pluginContainer.name = "EAP - Plugin not loaded...";

		System.Threading.Interlocked.Exchange(ref plugin, null);

		prePlugin.InstantiatePlugin(newPluginIdentifier);

        bool couldRestore = false;
        bool useDefault = !HasInstanceDataFor(prePlugin, false);
        prePlugin.DoActionSuspended(() => couldRestore = RestorePlugin(prePlugin, globalState || useDefault));

        if (Settings.Verbose)
            Debug.Log
            (
                "EAP.Component: Restoring "  +
                (globalState || useDefault ? "default " : "unique ") +
                serializedPlugin.Length + " bytes of state to the plugin? " + couldRestore
            );

		pluginCommand = newPluginIdentifier.Name;

		pluginContainer.name = "EAP." + pluginCommand;

		System.Threading.Interlocked.Exchange(ref plugin, prePlugin);

		if (showEditorDefault)
			ShowEditor();

		return true;
	}

	public bool ShowEditor()
	{
		if (PluginIsValid())
		{
			plugin.pluginInstance.ShowEditor();
			return true;
		}
		return false;
	}

	public double GetCPUUsage()
	{
		return cpuUsage;
	}

	bool IsConnected()
	{
		return plugin != null;
	}

    string GetInstanceDataPath(InternalBridgePlugin pluginToGenerate, bool enforceGlobalState = false)
    {
        return
            "Assets/External Audio Plugin/Instances/EAP_ps_" +
            pluginToGenerate.pluginInstance.GetID().Name.GetHashCode().ToString("x") + "_" +
            pluginToGenerate.pluginInstance.GetID().DescriptiveName.GetHashCode().ToString("x") + "_" +
            (enforceGlobalState ? "" : UniqueId) + ".bin";
    }

    private bool HasInstanceDataFor(InternalBridgePlugin pluginToRestore, bool useDefaultPreset)
    {
        if (pluginToRestore == null || !pluginToRestore.pluginInstance.HasValidPlugin())
            return false;

        if (!System.IO.File.Exists(GetInstanceDataPath(pluginToRestore, useDefaultPreset)))
            return false;

        return true;
    }

    private bool RestorePlugin(InternalBridgePlugin pluginToRestore, bool useDefaultInstanceData)
    {
        if (!HasInstanceDataFor(pluginToRestore, useDefaultInstanceData))
            return false;

        serializedPlugin = System.IO.File.ReadAllBytes(GetInstanceDataPath(pluginToRestore, useDefaultInstanceData));
            
        if (Settings.Verbose)
            Debug.Log("EAP.Component: Read " + serializedPlugin.Length + " bytes of state from disk");

        return serializedPlugin.Length > 0 && pluginToRestore.pluginInstance.Restore(serializedPlugin);
    }

	public void SerializePlugin(bool toDefaultPreset = false)
	{
		if (PluginIsValid())
		{
			serializedPlugin = plugin.pluginInstance.Serialize();
            if (Settings.Verbose)
                Debug.Log("EAP.Component: Got " + serializedPlugin.Length + " bytes of state from the plugin");

            if(serializedPlugin.Length > 0)
            {
                System.IO.File.WriteAllBytes(GetInstanceDataPath(plugin, toDefaultPreset), serializedPlugin);
            }
		}
	}

    public void ChangeUUID(string UUID)
    {
#if UNITY_EDITOR
        if (!String.IsNullOrEmpty(this.UniqueId))
            UniqueIdRegistry.Register(this.UniqueId, this.GetInstanceID());

        if (String.IsNullOrEmpty(UUID))
            UniqueId = UUID;

        UniqueIdRegistry.Register(this.UniqueId, this.GetInstanceID());
#endif
    }

    void OnDestroy()
	{
		SerializePlugin();

#if UNITY_EDITOR
        UniqueIdRegistry.Deregister(this.UniqueId);
#endif
    }

	void Start()
	{
#if UNITY_EDITOR
        if (String.IsNullOrEmpty(this.UniqueId))
        {
            UniqueId = UniqueIdRegistry.Generate();
            print("allocating " + this.UniqueId);
        }
        else
        {
            //print("already has " + this.UniqueId);
        }
 
        UniqueIdRegistry.Register(this.UniqueId, this.GetInstanceID());
#endif
        if(Application.isPlaying)
        {

            frameCounter = 0;
            if (instantiateOnRun)
            {
                InstantiatePlugin(pluginCommand);
            }
        }
        else
        {
            // TODO: implement this for out of gameplay - functionality
        }
    }


    void Update()
	{
		frameCounter++;

#if UNITY_EDITOR
        if (this.GetInstanceID() != UniqueIdRegistry.GetInstanceId(this.UniqueId))
        {
            UniqueId = UniqueIdRegistry.Generate();
            UniqueIdRegistry.Register(this.UniqueId, this.GetInstanceID());
        }
#endif

        if (PluginIsValid())
		{
			plugin.pluginInstance.Mix = mix;
			plugin.pluginInstance.Volume = volume;
		}
	}

    void Cloned()
    {
        print("cloned");
    }

	bool PluginIsValid()
	{
		var plug = this.plugin;
		return plug != null && plug.pluginInstance != null;
	}

	void OnAudioFilterRead(float[] data, int channels)
	{
		if (PluginIsValid())
		{
			watch.Reset();
			watch.Start();
			plugin.pluginInstance.ProcessInterleaved(data, channels);
			watch.Stop();
			var fractionateSecond = (data.Length/channels) / plugin.currentSampleRate;
			var curCpu = watch.Elapsed.TotalSeconds / fractionateSecond;
			curCpu *= 100;
			cpuUsage = curCpu + 0.98 * (cpuUsage - curCpu);
			// hack to make the editor update..
			cpu = cpuUsage;
		}
		else
		{
			cpuUsage = 0;
		}
	}

	void TryConnect()
	{
		
	}

	void Disconnect()
	{
		
	}
}
