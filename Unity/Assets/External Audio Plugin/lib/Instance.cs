using UnityEngine;
using System.Collections;
using System;

namespace EAP
{
	// TODO: Consider SafeHandle
	/// <summary>
	/// Read the ExternalAudioPlugin/Api.h to understand most of what is going on here
	/// </summary>
	public class Instance : IDisposable
	{
		public IntPtr Handle
		{
			get { return m_handle; }
		}

        public bool Suspended
        {
            get { return m_suspended; }
        }

		public string Name { get; private set; }
		public float Mix { get; set; }
		public float Volume { get; set; }

		private IntPtr m_handle;
		private float[] m_buffer;
		private Database.ID m_id;
		private bool m_suspended = true;
		public object ProcessorLock = new object();

		public Instance()
		{
			Mix = Volume = 1.0f;
			m_suspended = true;
			ProcessorLock = new object();
		}

        public Database.ID GetID()
        {
            return new Database.ID(m_id);
        }

		public bool LoadPlugin(Database.ID identifier)
		{
			if (identifier.IsValid && Binds.CreateFromID(identifier, ref m_handle) && HasValidPlugin())
			{
				Name = identifier.Name;
                m_id = identifier;
                if (Settings.Verbose)
                    Debug.Log("EAP: Loaded plugin " + Name);
				return true;
			}
			return false;
		}

		public byte[] Serialize()
		{
			CheckState();

			return Binds.GetSerializedPluginState(m_handle);
		}

		public bool Restore(byte[] data)
		{
			CheckState();

			return Binds.RestoreSerializedPluginState(m_handle, data);
		}

		public bool Resume()
		{
			CheckState();

			if(Binds.Resume(m_handle))
				m_suspended = false;

			return m_suspended == false;
		}

		public bool Suspend()
		{
			CheckState();

			if (Binds.Suspend(m_handle))
				m_suspended = true;

			return m_suspended == true;
		}

		public bool HasValidPlugin()
		{
			return m_handle != IntPtr.Zero;
		}

		public bool ShowEditor()
		{
			CheckState();

			return Binds.ShowEditor(m_handle);
		}

		public bool CloseEditor()
		{
			CheckState();

			return Binds.CloseEditor(m_handle);
		}

		public bool ResetIO(int inputBuffers, int outputBuffers, int estimatedBlockSize, double sampleRate)
		{
			CheckState();

			if (!m_suspended)
				throw new InvalidOperationException("EAP: I/O reset for plugin while it is not in a suspended state!");

			EnsureBuffer(estimatedBlockSize, inputBuffers > outputBuffers ? inputBuffers : outputBuffers);

			return Binds.ResetIO(m_handle, inputBuffers, estimatedBlockSize, sampleRate);
		}

		/// <summary>
		/// Acquires processor lock.
		/// </summary>
		public bool ProcessInterleaved(float[] interleavedData, int numChannels)
		{
			CheckState();

			if (m_suspended)
				return false;

			lock (ProcessorLock)
			{
				var numSamples = interleavedData.Length / numChannels;

				EnsureBuffer(numSamples, numChannels);

				for (int i = 0; i < numSamples; ++i)
				{
					for (int c = 0; c < numChannels; ++c)
					{
						m_buffer[i + c * numSamples] = interleavedData[i * numChannels + c];
					}
				}

				if (Binds.ProcessFloatReplacingSD(m_handle, m_buffer, numSamples, numChannels))
				{
					// copy samples back

					float rMix = 1.0f - Mix;
					float mix = Mix;
					float vol = Volume;
					float original, modified;
					for (int i = 0; i < numSamples; ++i)
					{
						for (int c = 0; c < numChannels; ++c)
						{
							original = interleavedData[i * numChannels + c];
							modified = m_buffer[i + c * numSamples];
							interleavedData[i * numChannels + c] = original * rMix + modified * vol * mix;
						}
					}

					return true;
				}
				else
				{
					throw new Exception("EAP: Some error occurred while processing.");
					float rMix = 1.0f - Mix;
					for (var i = 0; i < interleavedData.Length; i++)
					{
						interleavedData[i] *= rMix;
					}
				}
			}
			return false;
		}

		public bool Process(float[] data, int numChannels)
		{
			// TODO: support mix and volume here.
			CheckState();

			if (m_suspended)
				return false;

			var numSamples = data.Length / numChannels;
			lock (ProcessorLock)
			{
				return Binds.ProcessFloatReplacingSD(m_handle, m_buffer, numSamples, numChannels);
			}
		}

		private void CheckState()
		{
			if (!HasValidPlugin())
				throw new InvalidOperationException("Plugin " + Name + " is not valid.");
		}

		private void EnsureBuffer(int samples, int channels)
		{
			if (m_buffer == null || m_buffer.Length < samples * channels)
			{
				m_buffer = new float[samples * channels];
			}
		}

		/// <summary>
		/// This will disconnect and safely destroy the plugin in a thread-safe context, so long as you
		/// call this from the main thread (which you should always do), leaving the object in a disposed
		/// state.
		/// </summary>
		public void Dispose()
		{
			lock (ProcessorLock)
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing && HasValidPlugin())
			{
				Binds.Suspend(Handle);
				Binds.Release(ref m_handle);
                if (Settings.Verbose)
                    Debug.Log("EAP: Destroyed plugin " + Name);
			}
		}

		~Instance()
		{
			Dispose(true);
		}
	}
}
