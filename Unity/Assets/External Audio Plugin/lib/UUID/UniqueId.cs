﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class UniqueIdentifierAttribute : PropertyAttribute { }

public class UniqueId : MonoBehaviour
{
    [UniqueIdentifier]
    public string uniqueId;
}

public static class UniqueIdRegistry
{
    // TODO: use sparse set
    public static Dictionary<string, int> m_mapping = new Dictionary<string, int>();
    private static long m_gen = 0;
    public static void Deregister(String id)
    {
        m_mapping.Remove(id);
    }

    public static void Register(String id, Int32 value)
    {
        if (!m_mapping.ContainsKey(id))
            m_mapping.Add(id, value);
    }

    public static Int32 GetInstanceId(string id)
    {
        return m_mapping.ContainsKey(id) ? m_mapping[id] : 0;
    }

    public static string Generate()
    {
        long max = 0;
        foreach(var k in m_mapping.Keys)
        {
            max = System.Math.Max(long.Parse(k, System.Globalization.CultureInfo.CurrentCulture), max);
        }
        m_gen = max;
        return (++m_gen).ToString();
    }
}