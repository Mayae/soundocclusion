using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace EAP
{
	public class Database
	{
		public class ID : IComparable<ID>
		{
			public static ID Null = new ID("");

			public static ID FromPath(string path)
			{
				return new ID(path);
			}

			public ID(ID other)
			{
				m_id = other.m_id;
			}

			private ID(string name)
			{
				m_id = name;
			}
			// a non-specific readable name
			public string Name
			{
				get { return m_name ?? (m_name = Binds.NameOfID(m_id)); }
			}

			// a system- and runtime-specific string that uniquely describes this plugin
			public string DescriptiveName
			{
				get { return m_id; }
			}

			public bool IsValid
			{
				get { return this != Null && Instance().Contains(this); }
			}

			public int CompareTo(ID other)
			{
				return m_id.CompareTo(other.m_id);
			}

			public static bool operator == (ID left, ID right)
			{
				return 0 == left.CompareTo(right);
			}

			public static bool operator !=(ID left, ID right)
			{
				return !(left == right);
			}

			public override string ToString()
			{
				return "EAP.ID: " + (IsValid ? "Valid -> " : "Invalid -> ") + Name + " {" + DescriptiveName + "}";
			}

            public override bool Equals(object obj)
            {
                return this == (ID)obj;
            }

            public override int GetHashCode()
            {
                return m_id.GetHashCode();
            }

            private string m_name;
			private readonly string m_id;
		}

        public void Clear()
        {
            Binds.ClearKnownPlugins();
            UpdateDatabase();
        }

		public static Database Instance()
		{
			Database instance = s_instance;

			if (instance == null)
			{
				lock (s_lock)
				{
					instance = s_instance;
					if (instance == null)
					{
						s_instance = new Database();
					}
				}
			}
			return s_instance;
		}

		public int GetNumPluginIDs()
		{
			return m_pluginIDs.Count;
		}

		public ID GetPluginID(int index)
		{
			return new ID(m_pluginIDs[index]);
		}

		public bool Contains(ID possibleID)
		{
			return m_pluginIDs.Any(mPluginID => mPluginID == possibleID);
		}

		public ReadOnlyCollection<ID> GetList()
		{
			return m_pluginIDs.AsReadOnly();
		}

		public ID GetMatchingPluginIDFromName(string name)
		{
			lock (s_lock)
			{
				for (var i = 0; i < m_pluginIDs.Count; i++)
				{
					if (m_pluginIDs[i].Name == name)
					{
						return GetPluginID(i);
					}
				}
			}
			return ID.Null;
		}

		public int AddPluginsAtPath(string directPathOrDirectory)
		{
			Binds.AddPluginOrDirToKnownList(directPathOrDirectory);

			return UpdateDatabase();
		}

		public int AddInstalledPlugins()
		{
			Binds.ScanAndAddInstalledPlugins();

			return UpdateDatabase();
		}

		public void Save()
		{
			byte[] databaseContents = Binds.GetSerializedPluginDatabase();
			if (databaseContents != null && databaseContents.Length > 0)
			{
				

				System.IO.File.WriteAllBytes(DatabaseName, databaseContents);
				if (Settings.Verbose)
					Debug.Log("EAP: Deleted database, serialized " + databaseContents.Length + " bytes to file");
			}
			else
			{
				if (Settings.Verbose)
					Debug.Log("EAP: Deleted database, no content to serialize");
			}
		}

        private Database()
		{
			m_pluginIDs = new List<ID>();

			if(!System.IO.Directory.Exists(InstanceFolder))
			{
				System.IO.Directory.CreateDirectory(InstanceFolder);
			}

			if (System.IO.File.Exists(DatabaseName))
			{
				byte[] contents = System.IO.File.ReadAllBytes(DatabaseName);
				if (contents != null && contents.Length > 0 && Binds.RestoreSerializedPluginDatabase(contents))
				{
					UpdateDatabase();
					var plugs = GetNumPluginIDs();
                    if (Settings.Verbose)
                        Debug.Log("EAP: Restored database (" + plugs + " plugin" + (plugs != 1 ? "s" : "") + ")");
				}
			}
			else
			{
                if (Settings.Verbose)
                    Debug.Log("EAP: Created empty database");
			}
		}



		~Database()
		{
			Save();
		}
		private int UpdateDatabase()
		{
			lock (s_lock)
			{
				int before = GetNumPluginIDs();

				m_pluginIDs.Clear();

				foreach (string knownPluginID in Binds.GetKnownPluginIDs())
				{
					m_pluginIDs.Add(ID.FromPath(knownPluginID));
				}
				var newPlugs = GetNumPluginIDs() - before;
                if (Settings.Verbose)
                    Debug.Log("EAP: Updated database, added " + newPlugs + " plugin" + (newPlugs != 1 ? "s" : ""));

				return newPlugs;
			}
		}

        private const int s_constSeed = 5512;
		private const string DatabaseName = "Assets/External Audio Plugin/Instances/EAPSerializedDatabase.bin";
		private const string InstanceFolder = "Assets/External Audio Plugin/Instances";
		private static readonly object s_lock = new object();
		private static Database s_instance;
		private List<ID> m_pluginIDs;
	}
}
