using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using UnityEngine.UI;

namespace EAP
{
	using Ret = System.Int32;
	using Size_t = System.UInt64;


	public class Binds
	{
		[StructLayout(LayoutKind.Sequential)]
		private struct InternalPluginList
		{
			public Size_t numPlugins;
			public IntPtr plugins;

			public string[] ToArrayString()
			{
				string[] ret = new string[numPlugins];
				for (int i = 0; i < (int)numPlugins; ++i)
				{
					var stringPointer = Marshal.ReadIntPtr(plugins, i * Marshal.SizeOf(typeof(IntPtr)));
					ret[i] = Marshal.PtrToStringAnsi(stringPointer);
				}
				return ret;
			}
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct InternalBinaryBlob
		{
			public Size_t size;
			public IntPtr data;

			public byte[] ToByteBlob()
			{
				byte[] ret = new byte[size];
				Marshal.Copy(data, ret, 0, (int)size);
				return ret;
			}
		}

		public static string NameOfID(string pathOrIdentifier)
		{
			return System.IO.Path.GetFileNameWithoutExtension(pathOrIdentifier);
		}

		[Obsolete("Use EAP.Database instead")]
		public static bool IsIDValidPlugin(string pathOrIdentifier)
		{
			var plugs = GetKnownPluginIDs();
			if (plugs.Contains(pathOrIdentifier))
			{
				return true;
			}
			else
			{
				// only recursive (once) if the plugin is successfully added
				if(AddPluginOrDirToKnownList(pathOrIdentifier))
					return IsIDValidPlugin(pathOrIdentifier);
			}
			return false;
		}

		/// Return Type: EAPRet->int
		///utf8PathOrIdentifyingName: char*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_AddPluginsToKnownList", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_AddPluginsToKnownList([InAttribute()] [MarshalAsAttribute(UnmanagedType.LPStr)] string utf8PathOrIdentifyingName);

		public static bool AddPluginOrDirToKnownList(string pathOrIdentifyingName)
		{
			return EAP_AddPluginsToKnownList(pathOrIdentifyingName) == 1;
		}

		/// Return Type: EAPRet->int
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ScanAndAddInstalledPlugins", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ScanAndAddInstalledPlugins();

		public static int ScanAndAddInstalledPlugins()
		{
			return (int) EAP_ScanAndAddInstalledPlugins();
		}

		/// Return Type: EAPRet->int
		///pluginList: EAPPluginList*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_CreateKnownPluginIDs", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_CreateKnownPluginIDs(ref InternalPluginList pluginList);

		public static string[] GetKnownPluginIDs()
		{
			InternalPluginList list = new InternalPluginList();
			EAP_CreateKnownPluginIDs(ref list);
			if (list.numPlugins != 0)
			{
				var ret = list.ToArrayString();
				// TODO: check
				/*var error = */EAP_ReleasePluginList(ref list);
				return ret;
			}
			else
			{
				return new string[0];
			}
		}
		/// Return Type: EAPRet->int
		///pluginList: EAPPluginList*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ReleasePluginList", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ReleasePluginList(ref InternalPluginList pluginList);


		/// Return Type: EAPRet->int
		///serializedData: EAPBinaryBlob*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_CreateSerializedPluginDatabase", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_CreateSerializedPluginDatabase(ref InternalBinaryBlob serializedData);

		public static byte[] GetSerializedPluginDatabase()
		{
			InternalBinaryBlob blob = new InternalBinaryBlob();
			if (EAP_CreateSerializedPluginDatabase(ref blob) == 1)
			{
				if (blob.size > 0)
				{
					var ret = blob.ToByteBlob();
					// TODO: handle error
					/*var error = */EAP_ReleaseSerializedPluginDatabase(ref blob);
					return ret;
				}
			}

			return new byte[0];
		}

		/// Return Type: EAPRet->int
		///serializedData: EAPBinaryBlob*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_RestoreSerializedPluginDatabase", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_RestoreSerializedPluginDatabase(ref InternalBinaryBlob serializedData);

		public static bool RestoreSerializedPluginDatabase(byte[] serializedData)
		{
			InternalBinaryBlob blob = new InternalBinaryBlob();

			blob.size = (Size_t) serializedData.Length;
			blob.data = IntPtr.Zero;

			Ret returnCode = 0;

			try
			{
				blob.data = Marshal.AllocHGlobal(serializedData.Length);
				Marshal.Copy(serializedData, 0, blob.data, serializedData.Length);

				returnCode = EAP_RestoreSerializedPluginDatabase(ref blob);
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				if (blob.data != IntPtr.Zero)
				{
					Marshal.FreeHGlobal(blob.data);
				}
			}

			return returnCode == 1;
		}

		/// Return Type: EAPRet->int
		///serializedData: EAPBinaryBlob*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ReleaseSerializedPluginDatabase", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ReleaseSerializedPluginDatabase(ref InternalBinaryBlob serializedData);

        [DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ClearKnownPlugins", CallingConvention = CallingConvention.StdCall)]
        private static extern Ret EAP_ClearKnownPlugins();

        public static bool ClearKnownPlugins()
        {
            return EAP_ClearKnownPlugins() == 1;
        }

        /// Return Type: EAPRet->int
        ///utf8Name: char*
        ///pluginHandle: EAPPluginHandle*
        [DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_CreateFromID", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_CreateFromID([InAttribute()] [MarshalAsAttribute(UnmanagedType.LPStr)] string utf8Name, ref System.IntPtr pluginHandle);

		public static bool CreateFromID(Database.ID id, ref IntPtr pluginHandle)
		{
			return EAP_CreateFromID(id.DescriptiveName, ref pluginHandle) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		///numChannels: size_t->unsigned int
		///estimatedBlockSize: size_t->unsigned int
		///sampleRate: double
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ResetIO", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ResetIO(System.IntPtr handle, Size_t numChannels, Size_t estimatedBlockSize, double sampleRate);

		public static bool ResetIO(IntPtr handle, int numChannels, int estimatedBlockSize, double sampleRate)
		{
			return EAP_ResetIO(handle, (Size_t) numChannels, (Size_t) estimatedBlockSize, sampleRate) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_Suspend", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_Suspend(System.IntPtr handle);

		public static bool Suspend(IntPtr handle)
		{
			return EAP_Suspend(handle) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_Resume", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_Resume(System.IntPtr handle);

		public static bool Resume(IntPtr handle)
		{
			return EAP_Resume(handle) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ShowEditor", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ShowEditor(System.IntPtr handle);

		public static bool ShowEditor(IntPtr handle)
		{
			return EAP_ShowEditor(handle) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_CloseEditor", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_CloseEditor(System.IntPtr handle);

		public static bool CloseEditor(IntPtr handle)
		{
			return EAP_CloseEditor(handle) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		///serializedData: EAPBinaryBlob*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_CreateSerializedStateData", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_CreateSerializedStateData(System.IntPtr handle, ref InternalBinaryBlob serializedData);

		public static byte[] GetSerializedPluginState(IntPtr pluginHandle)
		{
			InternalBinaryBlob blob = new InternalBinaryBlob();
			if (EAP_CreateSerializedStateData(pluginHandle, ref blob) == 1)
			{
				if (blob.size > 0)
				{
					var ret = blob.ToByteBlob();
					// TODO: handle error
					var error = EAP_ReleaseSerializedStateData(pluginHandle, ref blob);
					return ret;
				}
			}

			return new byte[0];
		}
		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		///serializedData: EAPBinaryBlob*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_RestoreSerializedStateData", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_RestoreSerializedStateData(System.IntPtr handle, ref InternalBinaryBlob serializedData);

		public static bool RestoreSerializedPluginState(IntPtr handle, byte[] serializedData)
		{
			InternalBinaryBlob blob = new InternalBinaryBlob();

			blob.size = (Size_t)serializedData.Length;
			blob.data = IntPtr.Zero;

			Ret returnCode = 0;

			try
			{
				blob.data = Marshal.AllocHGlobal(serializedData.Length);
				Marshal.Copy(serializedData, 0, blob.data, serializedData.Length);

				returnCode = EAP_RestoreSerializedStateData(handle, ref blob);
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				if (blob.data != IntPtr.Zero)
				{
					Marshal.FreeHGlobal(blob.data);
				}
			}

			return returnCode == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle->void*
		///serializedData: EAPBinaryBlob*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ReleaseSerializedStateData", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ReleaseSerializedStateData(System.IntPtr handle, ref InternalBinaryBlob serializedData);


		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle*
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_Release", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_Release(ref System.IntPtr handle);

		public static bool Release(ref IntPtr pluginHandle)
		{
			if(Settings.Verbose)
                Debug.Log("EAP.Binds:Releasing");

			return EAP_Release(ref pluginHandle) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle
		///data: float**
		///numSamples: size_t->unsigned int
		///numChannels: size_t->unsigned int
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ProcessFloatReplacing", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ProcessFloatReplacing(System.IntPtr handle, [MarshalAs(UnmanagedType.LPArray)] float[][] data, Size_t numSamples, Size_t numChannels);

		public static bool ProcessFloatReplacing(IntPtr pluginHandle, float[][] data, int numSamples, int numChannels)
		{
			return EAP_ProcessFloatReplacing(pluginHandle, data, (Size_t)numSamples, (Size_t)numChannels) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle
		///data: float*
		///numSamples: size_t->unsigned int
		///numChannels: size_t->unsigned int
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ProcessFloatReplacingSD", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ProcessFloatReplacingSD(System.IntPtr handle, [MarshalAs(UnmanagedType.LPArray)] float[] data, Size_t numSamples, Size_t numChannels);

		public static bool ProcessFloatReplacingSD(IntPtr pluginHandle, float[] data, int numSamples, int numChannels)
		{
			return EAP_ProcessFloatReplacingSD(pluginHandle, data, (Size_t)numSamples, (Size_t)numChannels) == 1;
		}

        /// Return Type: EAPRet->int
        ///handle: EAPPluginHandle
        ///data: double**
        ///numSamples: size_t->unsigned int
        ///numChannels: size_t->unsigned int
        [DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ProcessDoubleReplacing", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ProcessDoubleReplacing(System.IntPtr handle, [MarshalAs(UnmanagedType.LPArray)] double[][] data, Size_t numSamples, Size_t numChannels);

		public static bool ProcessDoubleReplacing(IntPtr pluginHandle, double[][] data, int numSamples, int numChannels)
		{
			return EAP_ProcessDoubleReplacing(pluginHandle, data, (Size_t) numSamples, (Size_t) numChannels) == 1;
		}

		/// Return Type: EAPRet->int
		///handle: EAPPluginHandle
		///data: double*
		///numSamples: size_t->unsigned int
		///numChannels: size_t->unsigned int
		[DllImportAttribute("Assets/Plugins/ExternalAudioPluginBridge.dll", EntryPoint = "EAP_ProcessDoubleReplacingSD", CallingConvention = CallingConvention.StdCall)]
		private static extern Ret EAP_ProcessDoubleReplacingSD(System.IntPtr handle, [MarshalAs(UnmanagedType.LPArray)] double[] data, Size_t numSamples, Size_t numChannels);

		public static bool ProcessDoubleReplacingSD(IntPtr pluginHandle, double[] data, int numSamples, int numChannels)
		{
			return EAP_ProcessDoubleReplacingSD(pluginHandle, data, (Size_t)numSamples, (Size_t)numChannels) == 1;
		}
	}


}
