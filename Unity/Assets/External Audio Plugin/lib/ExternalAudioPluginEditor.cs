#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;



[CustomEditor(typeof(ExternalAudioPluginComponent))]
public class ExternalAudioPluginEditor : Editor
{

	public override void OnInspectorGUI()
	{
        EAP.Settings.Verbose = true;
		ExternalAudioPluginComponent plugin = (ExternalAudioPluginComponent) target;
		plugin.mix = EditorGUILayout.Slider("Dry/wet mix", plugin.mix, 0, 1);
		plugin.volume = EditorGUILayout.Slider("Volume", plugin.volume, 0, 1);

        var newUUID = EditorGUILayout.TextField("UID:", plugin.UniqueId);

        if(newUUID != plugin.UniqueId)
        {
            plugin.ChangeUUID(newUUID);
        }

        plugin.pluginCommand = EditorGUILayout.TextField(plugin.pluginCommand);

        // TODO: consider edit mode some day.
        if (Application.isPlaying && Application.isEditor)
		{
            EditorGUILayout.LabelField("Current plugin: " + plugin.CurrentName);
            EditorGUILayout.LabelField("Core usage: " + plugin.GetCPUUsage() + " %");

            if (GUILayout.Button("Instantiate command name"))
			{
				plugin.InstantiatePlugin(plugin.pluginCommand);
			}

			if (GUILayout.Button("Show editor"))
			{
				plugin.ShowEditor();
			}

            if (GUILayout.Button("Save state as default"))
            {
                plugin.SerializePlugin(true);
            }
        }

        if(GUILayout.Button("Scan command name"))
        {
            EAP.Database.Instance().AddPluginsAtPath(plugin.pluginCommand);
        }

        if(GUILayout.Button("Clear database"))
        {
            EAP.Database.Instance().Clear();
        }

		if (GUILayout.Button("Save database"))
		{
			EAP.Database.Instance().Save();
		}

		if (GUILayout.Button("Scan installed plugins"))
		{
			EAP.Database.Instance().AddInstalledPlugins();
		}

		var list = EAP.Database.Instance().GetList();

		string[] plugins = new string[EAP.Database.Instance().GetNumPluginIDs()];

		int selectedIndex = 0;

		for (var i = 0; i < list.Count; i++)
		{
			plugins[i] = list[i].Name;
			if (plugins[i] == plugin.pluginCommand)
				selectedIndex = i;
		}

		var newIndex = EditorGUILayout.Popup("Current plugins", selectedIndex, plugins);
		if (newIndex != selectedIndex)
		{
			plugin.pluginCommand = plugins[newIndex];
		}

		plugin.showEditorDefault = GUILayout.Toggle(plugin.showEditorDefault, "Always open editor");
		plugin.instantiateOnRun = GUILayout.Toggle(plugin.instantiateOnRun, "Instantiate on play");
        plugin.globalState = GUILayout.Toggle(plugin.globalState, "Enforce global state");
	}


}

#endif
