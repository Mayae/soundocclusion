﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace Sound.Behaviours
{
	/// <summary>
	/// Place top-most in hierachy to ensure its FixedUpdate is called first
	/// </summary>
	public class SoundEngine : MonoBehaviour
	{

		public static SoundEngine Instance { get; private set; }

		const float colorScale = 0.7f * 0.25f, colorOffset = 0.4f;
		const int colorMultiplier = 0x34729;

		public class Visibility : ICloneable
		{
			public object Clone()
			{
				return new Visibility { occluder = occluder, lineOfSight = lineOfSight, angle = angle};
			}

			public enum HitType
			{
				NotCalculated,
				Hit,
				Blocked,
				Occluded
			}

			public Surface occluder;
			public HitType lineOfSight;
			public Vector2 angle;
		}

		public List<Surface> GetSurfaces()
		{
			return m_surfaces;
		}

		public Dictionary<Surface, Dictionary<Surface, Visibility>> GetSurfaceMap()
		{
			return m_surfaceMap;
		}

		public SoundEngine()
		{
			Instance = this;
		}

		private void OnDrawGizmos()
		{
			return;
			var colours = m_surfaceMap.Count;

			var colorIndex = 1;
			foreach (var origin in m_surfaceMap)
			{
				var position = origin.Key.Position;

				colorIndex *= colorMultiplier;

				var colour = new Color((colorIndex & 3) * colorScale + colorOffset, ((colorIndex >> 4) & 3) * colorScale + colorOffset, ((colorIndex >> 9) & 3) * colorScale + colorOffset, 0.9f);

				foreach (var other in origin.Value)
				{

					if (other.Key == origin.Key)
						continue;

					if (other.Value.lineOfSight == Visibility.HitType.Hit)
					{
						var destination = other.Key.Position;

						Gizmos.color = Color.blue;

						Gizmos.DrawLine(position, destination);
					}
					else if (other.Value.lineOfSight == Visibility.HitType.Occluded)
					{
						var destination = other.Value.occluder.Position;

						Gizmos.color = Color.red;

						Gizmos.DrawLine(position, destination);
					}


				}
			}
		}

		private void FixedUpdate()
		{
			RaycastHit hit;

			// reset all sights
			foreach (var origin in m_surfaceMap)
			{
				foreach (var other in origin.Value)
				{
					other.Value.lineOfSight = Visibility.HitType.NotCalculated;
					other.Value.occluder = null;
				}
			}

			foreach (var origin in m_surfaceMap)
			{
				var position = origin.Key.Position;

				foreach (var other in origin.Value)
				{
					if (other.Key == origin.Key)
						continue;

					if (other.Value.lineOfSight != Visibility.HitType.NotCalculated)
						continue;

					var destination = other.Key.Position;
					var difference = destination - position;
					var direction = (difference).normalized;
					var distance = difference.magnitude;

					bool simpleCollision = true;
					Visibility.HitType hitType = Visibility.HitType.Blocked;

					Ray r = new Ray(position, direction);

					Surface possibleOccluder = null;

					//Mathf.Min(origin.Key.Size, other.Key.Size)
					if (distance <= 0)
					{
						hitType = Visibility.HitType.Hit;
					}
					else if (!Physics.Raycast(r, out hit, distance + 0.1f))
					{
						//Debug.Log("Unable to RayCast from " + origin.Key + " to " + other.Key);
					}
					else if (hit.collider.gameObject == other.Key.gameObject)
					{
						hitType = Visibility.HitType.Hit;
					}
					else if ((possibleOccluder = hit.collider.gameObject.GetComponent<Surface>()) != null)
					{
						hitType = Visibility.HitType.Occluded;
						// no line of sight requires us to cast a ray from the other side as well
						simpleCollision = false;
					}

					var a = other.Value;
					a.lineOfSight = hitType;
					a.occluder = possibleOccluder;

					if (simpleCollision)
					{
						// (there is line of sight)
						var b = m_surfaceMap[other.Key][origin.Key];

						b.lineOfSight = hitType;
						b.occluder = possibleOccluder;
					}




				}

			}
		}

		public void AddSoundSurface(Surface surface)
		{
			lock (GetInterceptingObject())
			{
				foreach (var pair in m_surfaceMap)
				{
					pair.Value.Add(surface, new Visibility());
				}

				Dictionary<Surface, Visibility> newMap = null;

				if (m_surfaceMap.Count == 0)
				{
					newMap = new Dictionary<Surface, Visibility>();
					newMap.Add(surface, new Visibility());
				}
				else
				{
					newMap = Utility.DeepDictionaryClone(m_surfaceMap.First().Value);
				}

				m_surfaceMap.Add(surface, newMap);
				m_surfaces.Add(surface);


				foreach (var n in m_notifs)
				{
					n.SoundSurfaceAdded(surface);
				}
			}
		}

		public void RemoveSoundSurface(Surface surface)
		{
			lock (GetInterceptingObject())
			{
				foreach (var pair in m_surfaceMap)
				{
					pair.Value.Remove(surface);
				}
				m_surfaceMap.Remove(surface);
				m_surfaces.Remove(surface);

				foreach (var n in m_notifs)
				{
					n.SoundSurfaceRemoved(surface);
				}
			}
		}

		public void AddSoundEmitter(SoundEmitter se)
		{
			lock (GetInterceptingObject())
			{
				foreach (var n in m_notifs)
				{
					n.SoundEmitterAdded(se);
				}
			}
		}

		public void RemoveSoundEmitter(SoundEmitter se)
		{
			lock (GetInterceptingObject())
			{
				foreach (var n in m_notifs)
				{
					n.SoundEmitterRemoved(se);
				}
			}
		}

		public void AddNotification(ISoundGraphNotification notif)
		{
			m_notifs.Add(notif);
		}

		public void RemoveNotification(ISoundGraphNotification notif)
		{
			m_notifs.Remove(notif);
		}

		public object GetInterceptingObject() { return m_lockObject; }
		public List<Surface> m_surfaces = new List<Surface>();
		Dictionary<Surface, Dictionary<Surface, Visibility>> m_surfaceMap = new Dictionary<Surface, Dictionary<Surface, Visibility>>();
		private List<ISoundGraphNotification> m_notifs = new List<ISoundGraphNotification>();
		private object m_lockObject = new object();
	}
}
