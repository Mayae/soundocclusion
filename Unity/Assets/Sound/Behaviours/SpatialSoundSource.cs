﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Sound
{
	interface ISpatialSoundSource
	{
		Vector3 Position { get; }
		Vector3 Velocity { get; }
	}

	public class SpatialMonoBehaviour : MonoBehaviour, ISpatialSoundSource
	{
		public Vector3 Position { get; set; }
		public Vector3 Velocity { get { return Vector3.zero; } }
		public float Size { get; protected set; }

		/// <summary>
		/// Unit is meters. A size of 0 models a point-source.
		/// </summary>
		public float spatialSize;
		public bool distanceScalesSquared = true;
		public bool inheritSizeFromTransform = true;
		public bool scaleDistanceAttenuationBySize = false;
		private bool m_launched;
		private Transform m_tsf;


		public virtual void MainUpdate()
		{

		}

		public virtual void Launch() { }

		void Update()
		{


			if(!m_launched)
			{
				m_launched = true;
				m_tsf = GetComponent<Transform>();
				Launch();
			}

			if(inheritSizeFromTransform)
			{
				var scale = m_tsf.lossyScale;
				Size = Mathf.Max(scale.x, scale.y, scale.z);
				spatialSize = Size;
			}
			else
			{
				Size = spatialSize;
			}

			Position = m_tsf.position;

			MainUpdate();
		}

	}

}
