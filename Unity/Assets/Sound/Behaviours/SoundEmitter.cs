﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;

namespace Sound.Behaviours
{
	public interface PreMainPostProcesser
	{
		void ProcessPrePass(int bufSize);
		void ProcessMainPass(float[] scratch1, float[] scratch2);
		void ProcessFinalize(int bufSize);
	}

	public abstract class SoundEmitter : SoundBase, PreMainPostProcesser
	{
		public bool StartOnAwake;

		protected bool m_isPlaying;
		protected FractionalDelay m_delayOutput;
		public virtual void OnAwake() { }

		public virtual FractionalDelay GetOutput()
		{
			return m_delayOutput;
		}

		public void Start()
		{
			AddSoundEmitter(this);
		}

		public virtual void OnDestroyed() { }

		public void OnDestroy()
		{
			RemoveSoundEmitter(this);
			OnDestroyed();
		}

		void Awake()
		{
			var config = AudioSettings.GetConfiguration();
			m_delayOutput = new FractionalDelay(config.sampleRate, config.dspBufferSize);
			OnAwake();
			if (StartOnAwake)
				StartPlaying();
		}

		public virtual void StartPlaying()
		{
			m_isPlaying = true;
		}

		public virtual void StopPlaying()
		{
			PausePlaying();
			ResetPlaying();
		}

		public virtual void PausePlaying()
		{
			m_isPlaying = false;
		}

		public virtual void ResetPlaying()
		{
		}

		public virtual bool IsPlaying()
		{
			return m_isPlaying;
		}

		public virtual void ProcessPrePass(int bufSize) { }
		public abstract void ProcessMainPass(float[] persistentScratch, float[] volatileScratch);
		public virtual void ProcessPass(int pass) { }
		public virtual void ProcessFinalize(int bufSize) { }
	}
}
