﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System;

namespace Sound
{

	public class SoundProperties : MonoBehaviour
	{
		public double SpeedOfSound;

		private static SoundProperties m_properties;

		/// <summary>
		/// Illegal to call before Awake() has run
		/// </summary>
		public static SoundProperties Instance()
		{
			if(m_properties == null)
			{
				var go = new GameObject("SoundPropertiesSurrogate");
				m_properties = go.AddComponent<SoundProperties>();
			}

			return m_properties;
		}

		void Awake()
		{
			if(m_properties != null && m_properties != this)
			{
				throw new InvalidProgramException("Multiple global sound properties");
			}

			m_properties = this;
		}

	}
}


