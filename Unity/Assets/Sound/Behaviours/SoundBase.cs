﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System.Collections.Generic;

namespace Sound.Behaviours
{

	public interface ISoundGraphNotification
	{
		void SoundEmitterAdded(SoundEmitter se);
		void SoundEmitterRemoved(SoundEmitter se);
		void SoundSurfaceAdded(Surface sc);
		void SoundSurfaceRemoved(Surface sc);
	}

	public class SoundBase : SpatialMonoBehaviour, ISoundGraphNotification
	{
		static public void AddSoundSurface(Surface sc)
		{
			SoundEngine.Instance.AddSoundSurface(sc);
		}

		static public void RemoveSoundSurface(Surface sc)
		{
			SoundEngine.Instance.RemoveSoundSurface(sc);
		}

		static public void AddSoundEmitter(SoundEmitter se)
		{
			SoundEngine.Instance.AddSoundEmitter(se);
		}

		static public void RemoveSoundEmitter(SoundEmitter se)
		{
			SoundEngine.Instance.RemoveSoundEmitter(se);
		}

		static public void AddNotification(ISoundGraphNotification notif)
		{
			SoundEngine.Instance.AddNotification(notif);
		}

		static public void RemoveNotification(ISoundGraphNotification notif)
		{
			SoundEngine.Instance.RemoveNotification(notif);
		}

		/// <summary>
		/// Intercepting object is locked when called
		/// </summary>
		public virtual void SoundEmitterAdded(SoundEmitter se) { }
		/// <summary>
		/// Intercepting object is locked when called
		/// </summary>
		public virtual void SoundEmitterRemoved(SoundEmitter se) { }

		/// <summary>
		/// Intercepting object is locked when called
		/// </summary>
		public virtual void SoundSurfaceAdded(Surface se) { }
		/// <summary>
		/// Intercepting object is locked when called
		/// </summary>
		public virtual void SoundSurfaceRemoved(Surface se) { }
	}
}
