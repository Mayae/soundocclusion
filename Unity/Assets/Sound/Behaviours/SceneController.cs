﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using UnityEngine.SceneManagement;

class SceneController : MonoBehaviour
{

	bool delaysDisabled;

	void OnGUI()
	{
		if(GUI.Button(new Rect(10, 10, 300, 30), "Click or press \'esc\' to go to next scene"))
		{
			SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
		}

		 if (GUI.Button(new Rect(10, 30, 300, 30), (delaysDisabled ? "Enable " : "Disable " ) + "delays in waves"))
		{
			delaysDisabled = !delaysDisabled;
			Sound.FractionalDelay.Tap.s_DisableDelays = delaysDisabled;
		}
	}


	void Update()
	{
		if(Input.GetKey(KeyCode.Escape))
		{
			SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
		}
	}
}
