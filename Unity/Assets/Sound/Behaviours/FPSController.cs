﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FPSController : MonoBehaviour
{
	public Vector3 eulerRotation;
	public bool Control;
	Sound.SpatialDownmixer mixer;
	private const float degreesToRadians = (float)(Math.PI / (180));

	private Rigidbody body;

	void Start()
	{
		body = GetComponent<Rigidbody>();
		mixer = GetComponent<Sound.SpatialDownmixer>();
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;
	}

	void Update()
	{
		var pos = Vector3.zero;

		pos.x += Input.GetAxis("Horizontal") * Time.deltaTime * 5;
		pos.z += Input.GetAxis("Vertical") * Time.deltaTime * 5;


		eulerRotation.y = Utility.RangeReduce(eulerRotation.y + Input.GetAxis("Mouse X") * 4, 360);
		eulerRotation.x = Utility.RangeReduce(eulerRotation.x - Input.GetAxis("Mouse Y") * 4, 360);

		if (mixer != null)
			mixer.eulerRotationRadians = eulerRotation * degreesToRadians;

		if(Control)
		{
			pos = transform.rotation * pos;
			if(body != null)
				body.AddForce(pos * 10, ForceMode.Impulse);
			else
				transform.position += pos;


			transform.localEulerAngles = eulerRotation;
		}
		else
			eulerRotation = transform.localEulerAngles;





	}

}
