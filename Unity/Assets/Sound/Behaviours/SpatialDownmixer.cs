﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using Sound.Behaviours;
using System;
using System.Collections.Generic;

namespace Sound
{

	public partial class SpatialDownmixer : SoundReceiver
	{
		public Vector3 eulerRotationRadians;
		public bool ignoreDistanceAttenuation;
		[Range(0, 1)]
		public float hrtfFilteringAmount = 1;
		[Range(0, 1)]
		public float interAuralFilterAmount = 1;
		[Range(0, 1)]
		public float sizeModulation = 1;
		public bool bypassFiltering;
		private float m_sampleRate;
		
		private float[][] downMix;
		private List<HRTFFilter> m_filters = new List<HRTFFilter>();

		bool go = false;

		public override void Launch()
		{
			var conf = AudioSettings.GetConfiguration();
			var buffers = new float[2][];
			buffers[0] = new float[conf.dspBufferSize];
			buffers[1] = new float[conf.dspBufferSize];
			m_sampleRate = conf.sampleRate;
			System.Threading.Thread.MemoryBarrier();
			downMix = buffers;

		}

		public void FixedUpdate()
		{
			if (Time.time > 1f)
				go = true;

			foreach (var f in m_filters)
			{
				f.ignoreDistanceAttenuation = ignoreDistanceAttenuation;
				f.hrtfAmount = hrtfFilteringAmount;
				f.iadAmount = interAuralFilterAmount;
				f.bypassFilters = bypassFiltering;
				f.sizeModulation = sizeModulation;
				f.Update(Position, eulerRotationRadians);
			}
		}

		public override void SoundEmitterRemoved(SoundEmitter se)
		{
			m_filters.RemoveAll(filter => filter.GetTarget() == se);
		}

		public override void SoundEmitterAdded(SoundEmitter se)
		{
			m_filters.Add(new HRTFFilter(se));
		}

		public override void SoundSurfaceRemoved(Surface sc)
		{
			m_filters.RemoveAll(filter => filter.GetTarget() == sc);
		}

		public override void SoundSurfaceAdded(Surface sc)
		{
			m_filters.Add(new HRTFFilter(sc));
		}

		public void OnAudioFilterRead(float[] data, int ch)
		{
			if (downMix == null)
				return;


			lock(SoundEngine.Instance.GetInterceptingObject())
			{
				System.Threading.Thread.MemoryBarrier();

				var numSamples = data.Length / ch;

				for (int i = 0; i < m_filters.Count; ++i)
				{
					m_filters[i].GetTargetProcessor().ProcessPrePass(numSamples);
				}

				for (int i = 0; i < m_filters.Count; ++i)
				{
					m_filters[i].PrepareProcess();
				}

				for (int i = 0; i < m_filters.Count; ++i)
				{
					m_filters[i].GetTargetProcessor().ProcessMainPass(downMix[0], downMix[1]);
				}

				for (int i = 0; i < m_filters.Count; ++i)
				{
					m_filters[i].DownMix(data);
				}

				for (int i = 0; i < m_filters.Count; ++i)
				{
					m_filters[i].GetTargetProcessor().ProcessFinalize(numSamples);
				}

			}
		}
	}
}