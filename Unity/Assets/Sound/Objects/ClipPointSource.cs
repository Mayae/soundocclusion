﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System;
using Sound.Behaviours;
using System.Collections.Generic;
using System.Linq;

namespace Sound
{
	public class ClipPointSource : SoundEmitter
	{
		public AudioClip clip;
		public bool Loop;
		public bool playQuantizedTestTone;
		public float offsetInSeconds;
		public float gain = 1;
		private int m_length;
		private int m_pos;
		private float[] m_data;

		private SoundMap.TreeNode m_soundTree;
		private SoundMap m_soundMap = new SoundMap();

		private Color m_color;
		private Color m_colorTransparent;
		private static int s_counter;
		private static float s_lowerCut = -92;
		private static float s_higherCut = 0;

		private static float s_systemBufSize = 1024, s_sampleRate = 48000;

		const float colorScale = 0.7f * 0.25f, colorOffset = 0.4f;
		const int colorMultiplier = 0x34729;

		class SurfaceEntry
		{
			public SurfaceFilter.State state;

			public SurfaceEntry(Surface surface)
			{
				se = surface;
				state = surface.GetSurfaceFilter().CreateState();
			}

			public void StartPass()
			{
				m_speed = 1.0f;
				state.StartNewPass();
			}

			public void SetDelayTo(float delayInSeconds)
			{
				// bug here, inconsistency between calls made to this and type of argument (meters vs speed)
				m_speed = 1.0f + (m_delay - delayInSeconds) / (s_systemBufSize / s_sampleRate);
				m_delay = delayInSeconds;
			}


			public float GetDelay() { return m_delay; }
			public float GetSpeed() { return m_speed; }

			Surface se;
			float m_delay, m_speed;

		}

		public void ReloadClip()
		{
			var sengine = SoundEngine.Instance;

			if(sengine != null)
			{
				lock(sengine.GetInterceptingObject())
				{
					LoadData();
				}
			}
		}

		private Dictionary<Surface, SurfaceEntry> m_filterStates = new Dictionary<Surface, SurfaceEntry>();

		public void FixedUpdate()
		{
			m_soundTree = m_soundMap.TraceSoundMap(Position);
		}

		public void OnDrawGizmos()
		{
			if (m_soundTree != null)
				RecursiveGizmoDraw(m_soundTree);
		}

		private void RecursiveGizmoDraw(SoundMap.TreeNode parent, float strength = 1.0f)
		{
			Vector3 parentPos;
			
			if (parent.payload.connection == null)
			{
				parentPos = Position;
			}
			else
			{
				parentPos = parent.payload.connection.Position;
			}


			for (int i = 0; i < parent.branches.Count; ++i)
			{
				var linkStrength = strength * parent.branches[i].payload.reflectionAngleAttenuation;

				var dbs = 20 * Mathf.Log10(linkStrength);

				var colorLerp = 1 - Utility.InvertLinear(dbs, s_lowerCut, s_higherCut);

				Gizmos.color = Color.Lerp(m_color, m_colorTransparent, colorLerp);
				Gizmos.DrawLine(parentPos, parent.branches[i].payload.connection.Position);
				RecursiveGizmoDraw(parent.branches[i], linkStrength);
			}

		}

		public ClipPointSource()
		{
			if(SoundEngine.Instance != null)
			{
				lock (SoundEngine.Instance.GetInterceptingObject())
				{
					foreach (var se in SoundEngine.Instance.GetSurfaces())
					{
						SoundSurfaceAdded(se);
					}
				}
			}
		}

		public override void SoundSurfaceAdded(Surface se)
		{
			m_filterStates.Add(se, new SurfaceEntry(se));
		}

		public override void SoundSurfaceRemoved(Surface se)
		{
			m_filterStates.Remove(se);
		}

		public override void OnDestroyed()
		{
			base.OnDestroyed();
			s_counter--;
		}

		public override void OnAwake()
		{
			AddNotification(this);
			s_counter++;

			int colorIndex = 1;
			unchecked
			{
				for (int i = 0; i < s_counter; ++i)
					colorIndex *= colorMultiplier;
			}


			m_color = new Color((colorIndex & 3) * colorScale + colorOffset, ((colorIndex >> 4) & 3) * colorScale + colorOffset, ((colorIndex >> 9) & 3) * colorScale + colorOffset, 0.9f);
			m_colorTransparent = m_color;
			m_colorTransparent.a = 0.0f;

			LoadData();

			var conf = AudioSettings.GetConfiguration();

			s_sampleRate = conf.sampleRate;
			s_systemBufSize = conf.dspBufferSize;

			ProcessFinalize((int)(offsetInSeconds * conf.sampleRate));
		}

		public override void MainUpdate()
		{
			GetOutput().Gain = gain;
		}

		public override void ProcessFinalize(int bufSize)
		{
			m_pos += bufSize;


			if (m_pos >= m_length)
			{
				if (Loop)
				{
					m_pos %= m_length;
				}
				else
					StopPlaying();
			}

			GetOutput().AdvanceCursor(bufSize);
		}

		public override void StartPlaying()
		{
			if (clip == null)
				throw new InvalidOperationException("Playing a null clip");

			if (clip.channels > 1)
				throw new InvalidOperationException("Only mono clips are supported");

			m_isPlaying = true;
		}

		public override void PausePlaying()
		{
			m_isPlaying = false;
		}

		public override void ResetPlaying()
		{
			m_pos = 0;
		}

		void EnsureDelayLineLengths(SoundMap.TreeNode node)
		{
			for (int i = 0; i < node.branches.Count; ++i)
			{
				var child = node.branches[i];

				RecursiveDelayLineDescent(child);

				var distance = child.payload.distance;

				var delay = FractionalDelay.Tap.DistanceToDelay(distance);

				GetOutput().EnsureDelayCapacity(delay);
			}

		}

		private void RecursiveDelayLineDescent(SoundMap.TreeNode node)
		{
			if (node == null)
				return;

			for (int i = 0; i < node.branches.Count; ++i)
			{
				var child = node.branches[i];

				EnsureDelayLineLengths(child);

				var distance = child.payload.distance;

				var delay = FractionalDelay.Tap.DistanceToDelay(distance);

				var surface = node.payload.connection;
				surface.GetOutputFromFace(surface.FaceFromPoint(child.payload.connection.Position)).EnsureDelayCapacity(delay);
			}

		}

		public override void ProcessPrePass(int bufSize)
		{
			if(m_soundTree != null)
			EnsureDelayLineLengths(m_soundTree);
			foreach (var filter in m_filterStates)
				filter.Value.StartPass();
		}


		public override void ProcessMainPass(float[] persistentScratch, float[] volatileScratch)
		{
			var localPos = m_pos;
			var remaining = persistentScratch.Length;

			if(playQuantizedTestTone)
			{
				for(int i = 0; i < remaining; ++i)
				{
					volatileScratch[i] = 0.25f*  Mathf.Sin((20 * Mathf.PI * i) / (remaining));
				}

				m_delayOutput.Insert(volatileScratch);
			}
			else
			{
				while (remaining > 0)
				{
					var available = m_length - localPos;

					var part = Utility.PartionedSaturatedSubtract(remaining, available);


					m_delayOutput.Insert(new ArraySegment<float>(m_data, localPos, part), persistentScratch.Length - remaining);

					localPos += part;
					localPos %= m_length;

					remaining -= part;
				}
			}
			PlayIntoTree(persistentScratch, volatileScratch, m_soundTree, 1.0f);

		}

		void PlayIntoTree(float[] signal, float[] volatileScratch, SoundMap.TreeNode node, float strength)
		{
			if (node == null)
				return;

			float[] input = null;

			if(node.payload.connection == null)
			{
				for (int i = 0; i < node.branches.Count; ++i)
				{
					var child = node.branches[i];
					var distance = child.payload.distance;
					var distanceFactor = distance;
					if (distanceScalesSquared)
						distanceFactor *= distance;

					if (scaleDistanceAttenuationBySize)
						distanceFactor /= Mathf.Max(1, spatialSize);


					var volume = 1.0f / (1 + distanceFactor);
					var delay = FractionalDelay.Tap.DistanceToDelay(distance);

					if (!m_filterStates.ContainsKey(child.payload.connection))
						continue;

					var filter = m_filterStates[child.payload.connection];

					if (child.payload.connection.receivesSoundTransparently)
					{
						filter.SetDelayTo(delay);

						GetOutput().Read(signal, filter.GetDelay(), filter.GetSpeed());
					}
					else
					{
						GetOutput().Read(signal, delay, 1);
					}

					// handle all visible surfaces firstly
					child.payload.connection.HandleIncomingSound(
						child.payload.entryFace,
						volume,
						signal,
						volatileScratch,
						filter.state
					);


				}
				for (int i = 0; i < node.branches.Count; ++i)
				{
					PlayIntoTree(signal, volatileScratch, node.branches[i], gain);
				}
			}
			else
			{

				var surface = node.payload.connection;

				if (!m_filterStates.ContainsKey(surface))
					return;

				surface.HandleIncomingSound(
					node.payload.entryFace,
					strength,
					signal,
					volatileScratch,
					m_filterStates[surface].state
				);

				for (int i = 0; i < node.branches.Count; ++i)
				{
					var child = node.branches[i];
					var distance = child.payload.distance;
					var distanceFactor = distance;
					if (surface.distanceScalesSquared)
						distanceFactor *= distance;

					if (surface.scaleDistanceAttenuationBySize)
						distanceFactor /= Mathf.Max(1, surface.spatialSize);

					var volume = 1.0f / (1 + distanceFactor);
					var delay = FractionalDelay.Tap.DistanceToDelay(distance);
					// fix this so it always works (associate taps to maps instead of filters - or fix bsp trees)
					if (child.payload.connection.receivesSoundTransparently)
					{

						if (!m_filterStates.ContainsKey(child.payload.connection))
							continue;

						var filter = m_filterStates[child.payload.connection];

						filter.SetDelayTo(delay);


						surface.GetOutputFromFace(surface.FaceFromPoint(child.payload.connection.Position)).Read(signal, filter.GetDelay(), filter.GetSpeed());
					}
					else
					{
						surface.GetOutputFromFace(surface.FaceFromPoint(child.payload.connection.Position)).Read(signal, delay, 1);
					}
					PlayIntoTree(signal, volatileScratch, child, volume);


				}
			}
		}

		void PlayIntoTree2(float[] persistentScratch, float[] volatileScratch, SoundMap.TreeNode parent, float accumulatedDistance = 0.0f)
		{
			if (parent == null)
				return;

			Vector3 parentPos;

			if (parent.payload.connection == null)
			{
				parentPos = Position;
			}
			else
			{
				parentPos = parent.payload.connection.Position;
			}


			for (int i = 0; i < parent.branches.Count; ++i)
			{
				var child = parent.branches[i];
				var distanceToChild = accumulatedDistance + Vector3.Distance(parentPos, child.payload.connection.Position);
				PlayIntoTree2(persistentScratch, volatileScratch, child, distanceToChild);

				var distanceScaleFactor = child.payload.connection.scaleDistanceAttenuationBySize ? 1.0f / child.payload.connection.Size : 1.0f;
				var childGain = 1.0f / (1 + distanceScaleFactor * distanceToChild * distanceToChild);

				GetOutput().ReadGained(persistentScratch, FractionalDelay.Tap.DistanceToDelay(distanceToChild), gain * childGain);

				child.payload.connection.GetOutputFromFace(Surface.Face.Front).InsertAdditive(persistentScratch);

			}


		}


		private void LoadData()
		{
			if (clip == null)
				throw new InvalidOperationException("Playing a null clip");

			if (clip.channels > 1)
				throw new InvalidOperationException("Only mono clips are supported");

			m_data = new float[clip.samples];
			m_length = clip.samples;
			m_pos = 0;
			clip.GetData(m_data, 0);

		}
	}
}


