﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;

namespace Sound
{
	class UnitSpherePanner : MonoBehaviour
	{
		public float distance;
		public float timeBetweenPan;
		public float smooth = 0.3f;
		private float m_lastPan;
		private Transform m_tsf;
		private float m_currentZ, m_currentX, m_currentY;
		private float m_targetZ, m_targetX, m_targetY;
		private float m_velZ, m_velX, m_velY;
		void Start()
		{
			m_tsf = GetComponent<Transform>();
		}

		void SetNewTarget()
		{
			m_targetX = UnityEngine.Random.Range(0, 360f);
			m_targetY = UnityEngine.Random.Range(0, 360f);
			m_targetZ = UnityEngine.Random.Range(0, 360f);
		}

		void Update()
		{

			if(Time.time > m_lastPan + timeBetweenPan)
			{
				m_lastPan = Time.time;
				SetNewTarget();
			}

			m_currentX = Mathf.SmoothDampAngle(m_currentX, m_targetX, ref m_velX, smooth);
			m_currentZ = Mathf.SmoothDampAngle(m_currentZ, m_targetZ, ref m_velZ, smooth);
			m_currentY = Mathf.SmoothDampAngle(m_currentY, m_targetY, ref m_velY, smooth);

			m_tsf.position = Quaternion.Euler(m_currentX, m_currentY, m_currentZ) * new Vector3(distance, 0, 0);
		}

	}
}
