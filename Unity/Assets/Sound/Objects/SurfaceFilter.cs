﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System;

namespace Sound
{
	public class SurfaceFilter : MonoBehaviour
	{

		public class Design
		{
			public OnePole.Coefficients lpCutoff;
			public StateVariableFilter.Coefficients[] filters;
			public float filterGain;
			public float inputGain;
			public float albedo;
		}

		public class State
		{
			public State(int nBands)
			{
				ic1eq = new float[nBands];
				ic2eq = new float[nBands];
			}

			public float[] ic1eq, ic2eq;
			public float lpz1;
			public bool rendered;


			public void StartNewPass()
			{
				rendered = false;
			}

		}

		public float inputGain = 1.0f;
		[Range(0, 1)]
		public float albedo = 1.0f;
		public float lowpassCutoff = 22000;
		public StateVariableFilter.PeakFilterDescription[] filters;
		public float mediumGain = 0.2f;

		private Design m_design;
		private float m_sampleRate;


		public void Filter(float strength, float[] input, float[] output, State state)
		{
			var filter = GetDesign();
			var size = input.Length;
			var fsize = filter.filters.Length;

			if (true)
			{

				var lpz1 = state.lpz1;

				var b1 = filter.lpCutoff.b1;
				var a0 = filter.lpCutoff.a0 * strength * filter.filterGain;

				// inject dc offset for denormalization purposes
				input[0] += 1e-10f;

				for (int i = 0; i < size; ++i)
				{
					lpz1 = input[i] * a0 + lpz1 * b1;
					output[i] = lpz1;
				}

				state.lpz1 = lpz1;
			}

			// parametric eq section

			for (int z = 0; z < fsize; ++z)
			{
				var ic1eq = state.ic1eq[z];
				var ic2eq = state.ic2eq[z];

				var a1 = filter.filters[z].a1;
				var a2 = filter.filters[z].a2;
				var a3 = filter.filters[z].a3;

				var m1 = filter.filters[z].m1;

				for (int i = 0; i < output.Length; i++)
				{
					var v3 = output[i] - ic2eq;
					var v1 = a1 * ic1eq + a2 * v3;
					var v2 = ic2eq + a2 * ic1eq + a3 * v3;

					ic1eq = 2.0f * v1 - ic1eq;
					ic2eq = 2.0f * v2 - ic2eq;

					output[i] += m1 * v1;

				}

				state.ic1eq[z] = ic1eq;
				state.ic2eq[z] = ic2eq;

			}
		}

		/// <summary>
		/// Must only be called from one thread
		/// </summary>
		public Design GetDesign()
		{
			if (m_design != null)
				return m_design;

			Design d = new Design();

			d.lpCutoff = OnePole.Design(OnePole.Type.LP, lowpassCutoff, m_sampleRate);
			d.filterGain = inputGain * mediumGain * (1 - albedo);
			d.inputGain = inputGain;
			d.filters = new StateVariableFilter.Coefficients[filters.Length];
			
			for(int i = 0; i < filters.Length; ++i)
			{
				d.filters[i] = StateVariableFilter.DesignBell(filters[i].centerFrequency / m_sampleRate, filters[i].quality, Mathf.Pow(10, filters[i].gainInDbs / 20));
			} 

			System.Threading.Thread.MemoryBarrier();
			return m_design = d;
		}

		public State CreateState()
		{
			return new Sound.SurfaceFilter.State(filters.Length);
		}

		void Awake()
		{
			m_sampleRate = AudioSettings.outputSampleRate;
		}
	}
}


