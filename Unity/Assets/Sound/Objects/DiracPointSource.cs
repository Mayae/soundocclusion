﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System;
using Sound.Behaviours;
using System.Collections.Generic;
using System.Linq;

namespace Sound
{
	public class DiracPointSource : SoundEmitter
	{
		public AudioClip clip;
		public bool Loop;
		public float offsetInSeconds;
		public float gain = 1;
		private int m_length;
		private int m_pos;
		private float[] m_data;

		private SoundMap.TreeNode m_soundTree;
		private SoundMap m_soundMap = new SoundMap();

		private Color m_color;
		private Color m_colorTransparent;
		private static int s_counter;
		private static float s_lowerCut = -92;
		private static float s_higherCut = 0;

		const float colorScale = 0.7f * 0.25f, colorOffset = 0.4f;
		const int colorMultiplier = 0x34729;


		private Dictionary<Surface, SurfaceFilter.State> m_filterStates = new Dictionary<Surface, SurfaceFilter.State>();

		public void FixedUpdate()
		{
			m_soundTree = m_soundMap.TraceSoundMap(Position);
		}

		public void OnDrawGizmos()
		{
			if (m_soundTree != null)
				RecursiveGizmoDraw(m_soundTree);
		}

		private void RecursiveGizmoDraw(SoundMap.TreeNode parent, float strength = 1.0f)
		{
			Vector3 parentPos;
			
			if (parent.payload.connection == null)
			{
				parentPos = Position;
			}
			else
			{
				parentPos = parent.payload.connection.Position;
			}


			for (int i = 0; i < parent.branches.Count; ++i)
			{
				var linkStrength = strength * parent.branches[i].payload.reflectionAngleAttenuation;

				var dbs = 20 * Mathf.Log10(linkStrength);

				var colorLerp = 1 - Utility.InvertLinear(dbs, s_lowerCut, s_higherCut);

				Gizmos.color = Color.Lerp(m_color, m_colorTransparent, colorLerp);
				Gizmos.DrawLine(parentPos, parent.branches[i].payload.connection.Position);
				RecursiveGizmoDraw(parent.branches[i], linkStrength);
			}

		}

		public override void SoundSurfaceAdded(Surface se)
		{
			m_filterStates.Add(se, se.GetSurfaceFilter().CreateState());
		}

		public override void SoundSurfaceRemoved(Surface se)
		{
			m_filterStates.Remove(se);
		}

		public override void OnDestroyed()
		{
			base.OnDestroyed();
			s_counter--;
		}

		public override void OnAwake()
		{
			AddNotification(this);
			s_counter++;

			int colorIndex = 1;
			unchecked
			{
				for (int i = 0; i < s_counter; ++i)
					colorIndex *= colorMultiplier;
			}


			m_color = new Color((colorIndex & 3) * colorScale + colorOffset, ((colorIndex >> 4) & 3) * colorScale + colorOffset, ((colorIndex >> 9) & 3) * colorScale + colorOffset, 0.9f);
			m_colorTransparent = m_color;
			m_colorTransparent.a = 0.0f;

			LoadData();

			var conf = AudioSettings.GetConfiguration();

			ProcessFinalize((int)(offsetInSeconds * conf.sampleRate));
		}

		public override void MainUpdate()
		{
			GetOutput().Gain = gain;
		}

		public override void ProcessFinalize(int bufSize)
		{

			GetOutput().AdvanceCursor(bufSize);
		}

		public override void StartPlaying()
		{
			m_isPlaying = true;
		}

		public override void PausePlaying()
		{
			m_isPlaying = false;
		}

		public override void ResetPlaying()
		{
			m_pos = 0;
		}

		void EnsureDelayLineLengths(SoundMap.TreeNode node)
		{
			if (node == null)
				return;


			for (int i = 0; i < node.branches.Count; ++i)
			{
				var child = node.branches[i];

				EnsureDelayLineLengths(child);
				if (node.payload.connection == null)
					continue;
				var distance = node.branches[i].payload.distance;

				var delay = FractionalDelay.Tap.DistanceToDelay(distance);

				var surface = node.payload.connection;
				surface.GetOutputFromFace(surface.FaceFromPoint(child.payload.connection.Position)).EnsureDelayCapacity(delay);
			}





		}

		public override void ProcessPrePass(int bufSize)
		{
			EnsureDelayLineLengths(m_soundTree);

		}

		int lol;

		public override void ProcessMainPass(float[] persistentScratch, float[] volatileScratch)
		{
			var localPos = m_pos;
			var remaining = persistentScratch.Length;

			for(int i = 0; i < persistentScratch.Length; ++i)
			{
				persistentScratch[i] = 0;
			}
			lol++;
			if(lol == 20)
				persistentScratch[0] = 1.0f;

			lol %= 20;

			m_delayOutput.Insert(persistentScratch);

			PlayIntoTree(persistentScratch, volatileScratch, m_soundTree);

		}

		public  void ProcessMainPass2(float[] persistentScratch, float[] volatileScratch)
		{
			var localPos = m_pos;
			var remaining = persistentScratch.Length;

			while (remaining > 0)
			{
				var available = m_length - localPos;

				var part = Utility.PartionedSaturatedSubtract(remaining, available);

				m_delayOutput.Insert(new ArraySegment<float>(m_data, localPos, part));

				localPos += part;
				localPos %= m_length;

				remaining -= part;
			}

			PlayIntoTree(persistentScratch, volatileScratch, m_soundTree);

		}

		void PlayIntoTree(float[] signal, float[] volatileScratch, SoundMap.TreeNode parent, float accumulatedDistance = 0.0f)
		{
			if (parent == null)
				return;

			float[] input = null;

			if(parent.payload.connection == null)
			{
				for (int i = 0; i < parent.branches.Count; ++i)
				{
					var child = parent.branches[i];
					var distance = child.payload.distance;

					var volume = 1.0f / (1 + distance * distance);

					GetOutput().ReadGained(signal, FractionalDelay.Tap.DistanceToDelay(distance), gain * volume);
					PlayIntoTree(signal, volatileScratch, child, 0);
				}
			}
			else
			{
				var surface = parent.payload.connection;

				surface.HandleIncomingSound(
					parent.payload.entryFace,
					1.0f,
					signal,
					volatileScratch,
					m_filterStates[parent.payload.connection]
				);

				for (int i = 0; i < parent.branches.Count; ++i)
				{
					var child = parent.branches[i];
					var distance = child.payload.distance;

					var volume = 1.0f / (1 + distance * distance);

					surface.GetOutputFromFace(surface.FaceFromPoint(child.payload.connection.Position)).ReadGained(signal, FractionalDelay.Tap.DistanceToDelay(distance), volume);
					PlayIntoTree(signal, volatileScratch, child, 0);


				}
			}
		}

		void PlayIntoTree2(float[] persistentScratch, float[] volatileScratch, SoundMap.TreeNode parent, float accumulatedDistance = 0.0f)
		{
			if (parent == null)
				return;

			Vector3 parentPos;

			if (parent.payload.connection == null)
			{
				parentPos = Position;
			}
			else
			{
				parentPos = parent.payload.connection.Position;
			}


			for (int i = 0; i < parent.branches.Count; ++i)
			{
				var child = parent.branches[i];
				var distanceToChild = accumulatedDistance + Vector3.Distance(parentPos, child.payload.connection.Position);
				PlayIntoTree2(persistentScratch, volatileScratch, child, distanceToChild);

				var distanceScaleFactor = child.payload.connection.scaleDistanceAttenuationBySize ? 1.0f / child.payload.connection.Size : 1.0f;
				var childGain = 1.0f / (1 + distanceScaleFactor * distanceToChild * distanceToChild);

				GetOutput().ReadGained(persistentScratch, FractionalDelay.Tap.DistanceToDelay(distanceToChild), gain * childGain);

				child.payload.connection.GetOutputFromFace(Surface.Face.Front).InsertAdditive(persistentScratch);

			}


		}


		private void LoadData()
		{

		}
	}
}


