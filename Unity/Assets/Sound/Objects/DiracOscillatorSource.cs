﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System;
using Sound.Behaviours;

namespace Sound
{
	class DiracOscillatorSource : SoundEmitter
	{
		public float frequency = 1.0f;
		private int m_count;
		private int m_trigger;
		public override void ProcessFinalize(int samples)
		{
			m_count += samples;
			m_trigger = Mathf.CeilToInt(GetOutput().SampleRate / frequency);

			GetOutput().AdvanceCursor(samples);
		}

		public override void ProcessMainPass(float[] scratch, float[] scratch2)
		{
			if (m_isPlaying)
			{
				for (int i = 0; i < scratch.Length; ++i)
				{
					scratch[i] = 0;
					if (i + m_count > m_trigger)
					{
						m_count = 0;
						scratch[i] = 1;
					}
				}

				GetOutput().Insert(scratch);
			}
		}

	}
}
