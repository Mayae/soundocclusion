﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Sound.Behaviours;

namespace Sound
{
	public class Surface : SoundBase, PreMainPostProcesser
	{
		public float Reflectivity { get { return GetSurfaceFilter().albedo; } }
		public bool sphericalRadiator;
		public bool receivesSoundTransparently;
		public SurfaceFilter surfaceFilter;

		public Vector3 Normal
		{
			get; private set;
		}
		public virtual void OnAwake() { }
		public virtual void OnDestroyed() { }

		public enum Face
		{
			Front, Back
		}

		public SurfaceFilter GetSurfaceFilter()
		{
			return surfaceFilter;
		}

		public static Face InverseFace(Face other)
		{
			if (other == Face.Front)
				return Face.Back;

			return Face.Front;
		}

		public Face FaceFromPoint(Vector3 point)
		{
			var angle = Vector3.Angle(Normal, point - Position);
			return angle > 90 ? Face.Back : Face.Front;


			//return m_plane.GetSide(Position - point) ? Face.Front : Face.Back;
		}

		public Face FaceFromPoint(Vector3 point, out float angleOut)
		{
			var angle = Vector3.Angle(Normal, point - Position);

			if (angle > 90)
			{
				angleOut = Mathf.Deg2Rad * (180 - angle);
				return Face.Back;
			}
			else
			{
				angleOut = Mathf.Deg2Rad * (angle);
				return Face.Front;
			}


			//return m_plane.GetSide(Position - point) ? Face.Front : Face.Back;
		}

		public FractionalDelay GetOutputFromFace(Face f)
		{
			return m_delays[(int)f];
		}

		public void HandleIncomingSound(Face f, float strength, float[] signal, float[] scratch, SurfaceFilter.State state)
		{

			if (state.rendered)
				return;
			state.rendered = true;

			var filter = GetSurfaceFilter().GetDesign();

			if (sphericalRadiator)
			{
				surfaceFilter.Filter(strength, signal, scratch, state);

				for (int i = 0; i < scratch.Length; ++i)
				{
					// flip sign of reflection
					scratch[i] += strength * -signal[i];
				}

				m_delays[0].InsertAdditive(scratch);
				m_delays[1].InsertAdditive(scratch);

				return;
			}


			if (filter.filterGain == 0.0f)
			{
				GetOutputFromFace(f).InsertAdditive(signal, strength * filter.inputGain);
				return;
			}

			surfaceFilter.Filter(strength, signal, scratch, state);

			GetOutputFromFace(InverseFace(f)).InsertAdditive(scratch);

			strength *= filter.inputGain;

			for (int i = 0; i < scratch.Length; ++i)
			{
				// flip sign of reflection
				scratch[i] += strength * -signal[i];
			}

			GetOutputFromFace(f).InsertAdditive(scratch);

		}

		public void Awake()
		{
			var sys = AudioSettings.GetConfiguration();
			for (int i = 0; i < m_delays.Length; ++i)
				m_delays[i] = new FractionalDelay(sys.sampleRate, sys.dspBufferSize);


			AddNotification(this);

			OnAwake();
		}

		public void Start()
		{
			AddSoundSurface(this);
		}

		public void OnDestroy()
		{
			OnDestroyed();
			RemoveSoundSurface(this);
			RemoveNotification(this);
		}

		public override void MainUpdate()
		{
			Normal = transform.rotation * Vector3.forward;
			m_plane = new Plane(Normal, Position.magnitude);
		}

		public virtual void ProcessPrePass(int bufSize) { }
		public virtual void ProcessMainPass(float[] scratch, float[] volatileScratch)
		{

		}
		public virtual void ProcessPass(int pass) { }
		public virtual void ProcessFinalize(int bufSize)
		{
			foreach(var fd in m_delays)
				if(fd != null)
					fd.AdvanceCursor(bufSize);
		}

		protected Plane m_plane;
		private FractionalDelay[] m_delays = new FractionalDelay[2];
	}

}
