﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sound
{
	class ArrayPool<T> where T : class
	{
		struct Entry
		{
			public List<T> thing;
			public bool occupied;
		}

		public List<T> Clone(T[] other, T exceptOne)
		{
			var list = Acquire(other.Length - 1);
			for(int a = 0, b = 0; a < other.Length; ++a)
			{
				if(other[a] != exceptOne)
				{
					list[b] = other[a];
					b++;
				}
			}

			return list;
		}

		public List<T> Clone(List<T> other, T exceptOne)
		{
			var list = Acquire(other.Count - 1);
			for (int a = 0, b = 0; a < other.Count; ++a)
			{
				if (other[a] != exceptOne)
				{
					list[b] = other[a];
					b++;
				}
			}

			return list;
		}

		public List<T> CloneAdd(T[] other, T added)
		{
			var list = Acquire(other.Length + 1);
			for (int i = 0; i < other.Length; ++i)
			{
				list[i] = other[i];
			}

			list[other.Length] = added;

			return list;
		}

		static public List<T> CloneOwned(List<T> other, T added)
		{
			var list = new List<T>(other.Count + 1);
			for (int i = 0; i < other.Count; ++i)
			{
				list.Add( other[i]);
			}

			list.Add(added);

			return list;
		}

		public List<T> CloneAdd(List<T> other, T added)
		{
			var list = Acquire(other.Count + 1);
			for (int i = 0; i < other.Count; ++i)
			{
				list[i] = other[i];
			}

			list[other.Count] = added;

			return list;
		}

		public List<T> Clone(T[] other, T exceptOne, T exceptTwo)
		{
			var list = Acquire(Math.Max(0, other.Length - 2));
			if (list.Count == 0)
				return list;

			for (int a = 0, b = 0; a < other.Length; ++a)
			{
				if (other[a] != exceptOne && other[a] != exceptTwo)
				{
					list[b] = other[a];
					b++;
				}
			}

			return list;
		}

		public List<T> Clone(List<T> other, T exceptOne, T exceptTwo)
		{
			var list = Acquire(Math.Max(0, other.Count - 2));
			if (list.Count == 0)
				return list;

			for (int a = 0, b = 0; a < other.Count; ++a)
			{
				if (other[a] != exceptOne && other[a] != exceptTwo)
				{
					try
					{
						list[b] = other[a];
					}
					catch (Exception e)
					{
						int k = 0;
					}
					b++;
				}
			}

			return list;
		}

		public List<T> Clone(T[] other)
		{
			var list = Acquire(other.Length);
			for (int i = 0; i < other.Length; ++i)
			{
				list[i] = other[i];
			}

			return list;
		}

		public List<T> Clone(List<T> other)
		{
			var list = Acquire(other.Count);
			for (int i = 0; i < other.Count; ++i)
			{
				try
				{
					list[i] = other[i];
				}
				catch (Exception e)
				{
					int k = 0;
				}
			}

			return list;
		}

		public List<T> Acquire(int size)
		{

			var it = BSearchFind(size);
			if (it != -1)
			{
				lists[it] = new Entry { thing = lists[it].thing, occupied = true };
				return lists[it].thing;
			}

			var entry = new Entry { thing = new List<T>(size), occupied = true };

			for (int i = 0; i < size; ++i)
				entry.thing.Add(null);

			lists.Add(entry);
			lists.Sort((a, b) => a.thing.Count - b.thing.Count);

			return entry.thing;
		}

		public void Release(List<T> obj)
		{
			var it = BSearchFind(obj);
			if(it != -1)
			{
				lists[it] = new Entry { thing = obj, occupied = false };
			}

		}


		public void Shrink()
		{
			lists.RemoveAll(e => !e.occupied);
		}

		private int BSearchFind(int size)
		{
			int mid;
			int low = 0;
			int high = lists.Count - 1;
			while (low <= high)
			{
				mid = (low + high) / 2;

				var count = lists[mid].thing.Count;

				if (count > size)
					high = mid - 1;
				else if (count < size)
					low = mid + 1;
				else
				{

					var it = mid;

					while(it >= 0)
					{
						if (!lists[it].occupied)
							return it;

						if (lists[it].thing.Count != size)
						{
							break;
						}

						--it;
					}

					it = mid + 1;

					while(it <= high)
					{
						if (!lists[it].occupied)
							return it;

						if (lists[it].thing.Count != size)
						{
							break;
						}

						++it;
					}
					return -1;
				}

			}
			return -1; //not found
		}


		private int BSearchFind(List<T> obj)
		{
			var size = obj.Count;

			int mid;
			int low = 0;
			int high = lists.Count - 1;
			while (low <= high)
			{
				mid = (low + high) / 2;

				var count = lists[mid].thing.Count;

				if (count > size)
					high = mid - 1;
				else if (count < size)
					low = mid + 1;
				else
				{

					var it = mid;

					while (it > 0)
					{
						if (lists[it].thing == obj)
							return it;


						if (lists[it].thing.Count != size)
						{
							break;
						}


						it--;
					}

					it = mid + 1;

					while (it < high)
					{
						if (lists[it].thing == obj)
							return it;


						if (lists[it].thing.Count != size)
						{
							break;
						}

						it++;

					}
					return -1;
				}

			}
			return -1; //not found
		}

		List<Entry> lists = new List<Entry>();
	}
}
