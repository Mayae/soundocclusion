﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections.Generic;
using System;
using Sound;

static public class Utility
{
	static public int PartionedSaturatedSubtract(int left, int right)
	{
		var part = left - (left > right ? left - right : 0);
		return part > right ? right : part;
	}

	static public float RangeReduce(float input, float domain)
	{
		while (input < 0)
			input += domain;

		while (input > domain)
			input -= domain;

		return input;
	}

	static public Vector2 RangeReduce(Vector2 input, float domain)
	{
		return new Vector2(RangeReduce(input.x, domain), RangeReduce(input.y, domain));
	}

	static public float DiffuseAngleAttenuation(float angle)
	{
		return 0.5f * (1 - Mathf.Cos(angle));
	}

	static public float InvertLinear(float y, float min, float max)
	{
		return (y - min) / (max - min);
	}

	/// <summary>
	/// Returns the distance from the point c to a vector defined from a to b
	/// </summary>
	static public float VectorPointDistance(Vector3 a, Vector3 b, Vector3 c)
	{
		return Vector3.Cross(b - a, a - c).magnitude / (b - a).magnitude;
	}

	public static Dictionary<TKey, TValue> DeepDictionaryClone<TKey, TValue>
		(Dictionary<TKey, TValue> original) where TValue : ICloneable
	{
		Dictionary<TKey, TValue> ret = new Dictionary<TKey, TValue>(original.Count,
																original.Comparer);
		foreach (KeyValuePair<TKey, TValue> entry in original)
		{
			ret.Add(entry.Key, (TValue)entry.Value.Clone());
		}
		return ret;
	}

	static public void SortListByDistance(List<Surface> map, Vector3 a, Vector3 b, bool forward = true)
	{
		if (forward)
			SortListByDistanceForward(map, a, b);
		else
			SortListByDistanceBackwards(map, a, b);
	}

	static public void SortListByDistance(List<Surface> map, Vector3 a, bool forward = true)
	{
		if (forward)
			SortListByDistanceForward(map, a);
		else
			SortListByDistanceBackwards(map, a);
	}

	private static void SortListByDistanceForward(List<Surface> map, Vector3 a, Vector3 b)
	{
		map.Sort(
			(x, y) =>
			{
				var A = a;
				var B = b;

				var distanceA = VectorPointDistance(a, b, x.Position);
				var distanceB = VectorPointDistance(a, b, y.Position);

				if (distanceA < distanceB)
					return -1;
				else if (distanceA == distanceB)
					return 0;
				return 1;
			}
		);
	}

	private static void SortListByDistanceBackwards(List<Surface> map, Vector3 a, Vector3 b)
	{
		map.Sort(
			(x, y) =>
			{
				var distanceA = VectorPointDistance(a, b, x.Position);
				var distanceB = VectorPointDistance(a, b, y.Position);

				if (distanceA > distanceB)
					return -1;
				else if (distanceA == distanceB)
					return 0;
				return 1;
			}
		);
	}

	private static void SortListByDistanceForward(List<Surface> map, Vector3 origin)
	{
		map.Sort(
			(x, y) =>
			{
				var distanceA = Vector3.Distance(origin, x.Position);
				var distanceB = Vector3.Distance(origin, y.Position);

				if (distanceA < distanceB)
					return -1;
				else if (distanceA == distanceB)
					return 0;
				return 1;
			}
		);
	}

	private static void SortListByDistanceBackwards(List<Surface> map, Vector3 origin)
	{
		map.Sort(
			(x, y) =>
			{
				var distanceA = Vector3.Distance(origin, x.Position);
				var distanceB = Vector3.Distance(origin, y.Position);

				if (distanceA > distanceB)
					return -1;
				else if (distanceA == distanceB)
					return 0;
				return 1;
			}
		);
	}

}
