﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System;
using UnityEngine;
using Sound.Behaviours;

namespace Sound
{

	class HRTFFilter
	{
		public float leftCutoff, rightCutoff;
		public float leftHz, rightHz;
		public float angle;
		public float earSeparationInMeters = 0.18f;
		public float modelledNyquist = 24000;
		public float sizeModulation;
		/// <summary>
		/// How much HRTF filtering is applied. 1 equals complete point-source behaviour, while 0 equals zero filtering applied
		/// </summary>
		public float hrtfAmount = 1.0f;
		/// <summary>
		/// How much modelling is applied to differentiate the ears. 
		/// This includes inter-aural time difference (ITD) and inter-aural level difference (ILD)
		/// </summary>
		public float iadAmount = 1.0f;
		public bool ignoreDistanceAttenuation = false;
		public bool bypassFilters = false;

		struct ElevationDelay
		{
			public float angularDistance;
			public float strength;
		}

		private class FilterSettings
		{
			public float leftGain;
			public float rightGain;
			public float rightDistance, leftDistance;
			public ElevationDelay elevationDelay;
			public FractionalDelay outputDelay;
			public StateVariableFilter.Coefficients pinnaLeft, pinnaRight;
			public OnePole.Coefficients backLeft, backRight, positiveElevation, negativeElevation;
		}

		

		public HRTFFilter(SoundEmitter se)
		{
			m_target = se;
			var conf = AudioSettings.GetConfiguration();
			m_leftSource = new float[conf.dspBufferSize];
			m_rightSource = new float[conf.dspBufferSize];

			m_mainTapLeft = new FractionalDelay.Tap(se.GetOutput());
			m_mainTapRight = new FractionalDelay.Tap(se.GetOutput());

			m_pinnaTapLeft = new FractionalDelay.Tap(se.GetOutput());
			m_pinnaTapRight = new FractionalDelay.Tap(se.GetOutput());

			m_possibleEmitter = se;
			m_processor = se;
		}

		public HRTFFilter(Surface sc)
		{
			m_target = sc;
			var conf = AudioSettings.GetConfiguration();
			m_leftSource = new float[conf.dspBufferSize];
			m_rightSource = new float[conf.dspBufferSize];

			m_mainTapLeft = new FractionalDelay.Tap(sc.GetOutputFromFace(Surface.Face.Front));
			m_mainTapRight = new FractionalDelay.Tap(sc.GetOutputFromFace(Surface.Face.Front));

			m_pinnaTapLeft = new FractionalDelay.Tap(sc.GetOutputFromFace(Surface.Face.Front));
			m_pinnaTapRight = new FractionalDelay.Tap(sc.GetOutputFromFace(Surface.Face.Front));

			m_possibleSurface = sc;
			m_processor = sc;
		}

		public SoundBase GetTarget()
		{
			return m_target;
		}

		public PreMainPostProcesser GetTargetProcessor()
		{
			return m_processor;
		}

		public void Update(Vector3 pos, Vector3 headEulerRadians)
		{
			FilterSettings fs = new FilterSettings();



			const float twoPi = 2 * Mathf.PI;
			var difference = m_target.Position - pos;
			var size = sizeModulation * m_target.Size * 0.5f;

			Vector2 rotation;

			var sampleRate = AudioSettings.GetConfiguration().sampleRate;

			// model size and filtering amount
			var horizontalDistance = Mathf.Max(0.0001f, Mathf.Sqrt(difference.z * difference.z + difference.x * difference.x) - size);
			var verticalDistance = Mathf.Max(0.0001f, Mathf.Sqrt(difference.y * difference.y + difference.z * difference.z) - size);


			// calculate rotation angles in a 2D coordinate system
			// relies on not being able to rotate around X (ie vertically) more than pi * 0.5 +/-
			rotation.x = Utility.RangeReduce(Mathf.Atan2(difference.z, difference.x) + headEulerRadians.y, twoPi);

			var reducedY = headEulerRadians.x;
			//if (reducedY > Mathf.PI)
			//	reducedY = 2 * Mathf.PI - reducedY;

			// ughhh
			rotation.y = Utility.RangeReduce(Mathf.Atan2(horizontalDistance, difference.y) - headEulerRadians.x, twoPi);
			if (rotation.y > Mathf.PI)
				rotation.y = 2 * Mathf.PI - rotation.y;

			var filterAmount = Mathf.Clamp01(1 - hrtfAmount);
			// * 0.5 to account for radius vs. size
			// 2 * to map atan() to 0 .. 1
			var horizontalObstruction = 2 * Mathf.Atan(size / horizontalDistance) / Mathf.PI;
			var horizontalModulation = filterAmount + hrtfAmount * horizontalObstruction;
			var verticalModulation = filterAmount + hrtfAmount * 2 * Mathf.Atan(size / verticalDistance) / Mathf.PI;


			var direction = (difference).normalized;
			var distance = difference.magnitude;

			Ray r = new Ray(pos, direction);
			RaycastHit hit;

			// TODO: detect when inside objects?
			// TODO: FAILS on moving objects. investigate physics and callbacks. (are we inside physics or something?)

			bool visible = false; 
			float surfaceAngleAttenuation = 1.0f;

			if (m_possibleSurface != null)
			{
				if(!m_possibleSurface.receivesSoundTransparently)
				{
					visible = Physics.Raycast(r, out hit, distance) && hit.collider.gameObject == m_target.gameObject;
				}
				else
				{

					visible = !Physics.Linecast(pos, m_target.Position, ~((1 << 2) | (1 << 8)));
				}
				float outAngle;
				fs.outputDelay = m_possibleSurface.GetOutputFromFace(m_possibleSurface.FaceFromPoint(pos, out outAngle));
				if(!m_possibleSurface.sphericalRadiator)
					surfaceAngleAttenuation = Mathf.Abs(Mathf.Cos(outAngle));
			}
			else
			{
				visible = Physics.Raycast(r, out hit, distance) && hit.collider.gameObject == m_target.gameObject;
				fs.outputDelay = m_possibleEmitter.GetOutput();
			}


			{
				

				// model ear separation in distance
				// this is used for inter-aural delay
				// first start with a normalized vector that represents the right ear:
				var x = earSeparationInMeters;
				var y = 0f;
				// normalize angle to coordinate system (so 0 = no rotation)
				var rotationAngle = -headEulerRadians.y;

				// make rotation matrix:
				var cosine = Mathf.Cos(rotationAngle);
				var sine = Mathf.Sin(rotationAngle);

				// rotate matrix
				var real = x * cosine - y * sine;
				var imag = x * sine + y * cosine;

				var oldLeft = new Vector3(difference.x + real, difference.y, difference.z + imag);
				// the left response is completely symmetrical around both axii, so derive that:
				var oldRight = new Vector3(difference.x - real, difference.y, difference.z - imag);

				var leftPos = new Vector3(pos.x - real, pos.y, pos.z - imag);
				var rightPos = new Vector3(pos.x + real, pos.y, pos.z + imag);

				var centralDistance = Vector3.Distance(pos, m_target.Position);

				// the delay between the ears is a factor of inter-aural time difference and how large the object is in our view
				var separationFactor = iadAmount * (1 - horizontalObstruction);

				// these values are used to determine delay
				fs.leftDistance = Vector3.Distance(leftPos, m_target.Position) * separationFactor + centralDistance * (1 - separationFactor);
				fs.rightDistance = Vector3.Distance(rightPos, m_target.Position) * separationFactor + centralDistance * (1 - separationFactor);

				if(!visible)
				{
					fs.rightGain = 0;
					fs.leftGain = 0;
				}
				else if(!ignoreDistanceAttenuation)
				{
					// model different attenuation between ears
					// take size into account
					var leftAttDistance = Mathf.Max(fs.leftDistance - size, 0) ;
					var rightAttDistance = Mathf.Max(fs.rightDistance - size, 0);
					var distanceScaleFactor = m_target.scaleDistanceAttenuationBySize ? 1.0f / Mathf.Max(1, size) : 1.0f;

					if(m_target.distanceScalesSquared)
					{
						leftAttDistance *= leftAttDistance;
						rightAttDistance *= rightAttDistance;
					}

					fs.leftGain = surfaceAngleAttenuation / (1 + distanceScaleFactor * leftAttDistance);
					fs.rightGain = surfaceAngleAttenuation / (1 + distanceScaleFactor * rightAttDistance);
				}
				else
				{
					fs.rightGain = 1.0f;
					fs.leftGain = 1.0f;
				}
			}

			UpdateFrontBackFilters(fs, rotation.x, sampleRate, modelledNyquist, 600, horizontalModulation);
			UpdateElevationFilters(fs, rotation.y, sampleRate, modelledNyquist, 600, verticalModulation);
			//UpdatePinnaFilters(fs, rotation.x, sampleRate, modelledNyquist, 100, 1000, horizontalModulation);

			angle = rotation.x;


			System.Threading.Thread.MemoryBarrier();
			settings = fs;

		}


		private void UpdatePinnaFilters(FilterSettings fs, float rotation, float sampleRate, float modelledNyquist, float lowerCutoff, float higherCutoff, float dMod)
		{
			// 1.0 / sqrt(2)
			const float butterworthQuality = 0.70710678118654752440084436210485f;

			// provides -6dB dip linear gain smoothly rotating in right hemisphere with angle = pi / 2 equals -3dB, angle = pi = -0dB, angle = 0 = -6dB (for left channel)
			var cosLx = 1.0f - Mathf.Cos(rotation);
			var cosRx = 1.0f - Mathf.Cos(Mathf.PI + rotation);

			var dipStrengthLeft = 0.25f * cosLx + 0.5f;
			var dipStrengthRight = 0.25f * cosRx + 0.5f;

			var cutoffLeft = 1.0f - cosLx;
			var cutoffRight = 1.0f - cosRx;

			leftCutoff = dipStrengthLeft;
			rightCutoff = dipStrengthRight;

			leftHz = lowerCutoff * Mathf.Pow(higherCutoff / lowerCutoff, dMod + (1 - dMod) * cutoffLeft);
			rightHz = lowerCutoff * Mathf.Pow(higherCutoff / lowerCutoff, dMod + (1 - dMod) * cutoffRight);

			fs.pinnaLeft = StateVariableFilter.DesignBell(leftHz / sampleRate, butterworthQuality, dMod + (1 - dMod) * dipStrengthLeft);
			fs.pinnaRight = StateVariableFilter.DesignBell(rightHz / sampleRate, butterworthQuality, dMod + (1 - dMod) * dipStrengthRight);
		}


		private void UpdateFrontBackFilters(FilterSettings fs, float rotation, float sampleRate, float modelledNyquist, float lfCutoff, float dMod)
		{
			var rightAngle = (Mathf.PI - rotation);
			// this factor makes modulation less apparent when close to objects.
			leftCutoff = dMod + (1 - dMod) * AngleToBackfrontCutoff(rotation);
			rightCutoff = dMod + (1 - dMod) * AngleToBackfrontCutoff(Utility.RangeReduce(rightAngle, Mathf.PI * 2));
			// design front-to-back filters
			leftHz = lfCutoff * Mathf.Pow(modelledNyquist / lfCutoff, leftCutoff);
			rightHz = lfCutoff * Mathf.Pow(modelledNyquist / lfCutoff, rightCutoff);
			fs.backLeft = OnePole.Design(OnePole.Type.LP, leftHz, sampleRate);
			fs.backRight = OnePole.Design(OnePole.Type.LP, rightHz, sampleRate);
		}

		private static void UpdateElevationFilters(FilterSettings fs, float rotation, float sampleRate, float modelledNyquist, float hfCutoff, float dMod)
		{
			const float upDistance = 0.025725f; // delay ms = 0.075 * speed of sound / 1000 (invariant here, TODO:)
			const float bottomDelay = 0.1f; // delay = 0.035 (well, a bit less after fine-tuning)
			const float strengthTop = 0.1f;
			const float strengthBottom = 0.35f;

			ElevationDelay d;

			var linearPositionFromTop = Utility.InvertLinear(rotation, 0, Mathf.PI);

			d.angularDistance = Mathf.Lerp(upDistance, bottomDelay, linearPositionFromTop);
			d.strength = (1 - dMod) * Mathf.Lerp(strengthTop, strengthBottom, linearPositionFromTop);

			fs.elevationDelay = d;

			var positiveAngle = Mathf.PI - rotation;

			var arg = (1 - dMod) * 0.5f * (1 - (Mathf.Cos(positiveAngle)));
			var positiveHz = 4 * Mathf.Pow(hfCutoff / 4, arg);
			positiveHz = Mathf.Min(positiveHz, sampleRate * 0.5f);

			fs.positiveElevation = OnePole.Design(OnePole.Type.LP, positiveHz, sampleRate);
		}

		/// <summary>
		/// Domain: 0 .. 2 pi
		/// </summary>
		/// <param name="angle"></param>
		/// <returns></returns>
		static public float AngleToBackfrontCutoff(float angle)
		{
			if (angle < Mathf.PI * 0.5)
				return 0.5f * (1 - Mathf.Cos(2 * angle));
			else if (angle < Mathf.PI * 1.25)
				return 1;
			else
			{
				var x = (angle - Mathf.PI * 1.25f) / (Mathf.PI * 0.75f);
				x = x - 1;
				x = 1 - x * x * x;
				return (1 - Mathf.Cos(Mathf.PI + Mathf.PI * x)) * 0.5f;
			}

		}
		
		public void PrepareProcess()
		{
			m_processLocalSettings = settings;
			if(m_processLocalSettings != null)
			{
				m_mainTapLeft.SetTarget(m_processLocalSettings.outputDelay);
				m_mainTapRight.SetTarget(m_processLocalSettings.outputDelay);

				m_pinnaTapLeft.SetTarget(m_processLocalSettings.outputDelay);
				m_pinnaTapRight.SetTarget(m_processLocalSettings.outputDelay);

				// update ear delays
				m_mainTapLeft.SetDelayFromDistance(m_processLocalSettings.leftDistance);
				m_mainTapRight.SetDelayFromDistance(m_processLocalSettings.rightDistance);

				m_pinnaTapLeft.SetDelayFromDistance(m_processLocalSettings.leftDistance + m_processLocalSettings.elevationDelay.angularDistance);
				m_pinnaTapRight.SetDelayFromDistance(m_processLocalSettings.rightDistance + m_processLocalSettings.elevationDelay.angularDistance);
			}

		}

		public void DownMix(float[] dest)
		{
			if (m_processLocalSettings != null)
			{
				int length = dest.Length >> 1;
				// read delay-lines

				if(bypassFilters)
				{
					m_processLocalSettings.outputDelay.Read(m_leftSource, 0);
					m_processLocalSettings.outputDelay.Read(m_rightSource, 0);

					var gain = 0.0f;

					if (m_possibleEmitter == null)
						gain = 1;

					for (int i = 0; i < m_leftSource.Length; ++i)
					{
						dest[i * 2] += m_leftSource[i] * gain;
						dest[i * 2 + 1] += m_rightSource[i] * gain;
					}

					return;
				}

				//Array.Clear(m_leftSource, 0, m_leftSource.Length);
				//Array.Clear(m_rightSource, 0, m_leftSource.Length);
				m_mainTapLeft.Read(m_leftSource);
				m_mainTapRight.Read(m_rightSource);

				m_pinnaTapLeft.ReadAdditively(m_leftSource, m_processLocalSettings.elevationDelay.strength);
				m_pinnaTapRight.ReadAdditively(m_rightSource, m_processLocalSettings.elevationDelay.strength);


				unchecked
				{
					var lpTC = m_processLocalSettings.positiveElevation;
					var lpBC = m_processLocalSettings.negativeElevation;

					var z1PosLeft = m_posElevationLeft.z1;
					var z1PosRight = m_posElevationRight.z1;

					for (int i = 0; i < length; ++i)
					{
						var leftIn = m_leftSource[i];
						var rightIn = m_rightSource[i];

						// elevation section
						z1PosLeft = leftIn * lpTC.a0 + z1PosLeft * lpTC.b1;
						z1PosRight = rightIn * lpTC.a0 + z1PosRight * lpTC.b1;

						m_leftSource[i] = (leftIn - z1PosLeft);
						m_rightSource[i] = (rightIn - z1PosRight);

					}
					m_posElevationLeft.z1 = z1PosLeft;
					m_posElevationRight.z1 = z1PosRight;
				}


				// front-back section
				unchecked
				{
					var lpLC = m_processLocalSettings.backLeft;
					var lpRC = m_processLocalSettings.backRight;

					var lpLS = m_backLeft;
					var lpRS = m_backRight;

					var diffLGain = m_processLocalSettings.leftGain - m_oldGainLeft;
					var diffRGain = m_processLocalSettings.rightGain - m_oldGainRight;

					var step = 1.0f / length;
					var accum = 0.0f;

					for (int i = 0; i < length; ++i)
					{
						var left = m_leftSource[i] * (m_oldGainLeft + diffLGain * accum);
						var right = m_rightSource[i] * (m_oldGainRight + diffRGain * accum);

						lpLS.z1 = left * lpLC.a0 + lpLS.z1 * lpLC.b1;
						lpRS.z1 = right * lpRC.a0 + lpRS.z1 * lpRC.b1;

						//m_leftSource[i] = lpLS.z1;
						//m_rightSource[i] = lpRS.z1;
						accum += step;

						// changed!!
						dest[i * 2] += lpLS.z1;
						dest[i * 2 + 1] += lpRS.z1;
					}

					m_backLeft = lpLS;
					m_backRight = lpRS;

					m_oldGainLeft = m_processLocalSettings.leftGain;
					m_oldGainRight = m_processLocalSettings.rightGain;

				}


				// outer-ear phase section (pinna emulation)

				if(false)
				{
					var ic1eqL = m_pinnaLeft.ic1eq;
					var ic2eqL = m_pinnaLeft.ic2eq;

					var a1L = m_processLocalSettings.pinnaLeft.a1;
					var a2L = m_processLocalSettings.pinnaLeft.a2;
					var a3L = m_processLocalSettings.pinnaLeft.a3;


					var m1L = m_processLocalSettings.pinnaLeft.m1;

					var ic1eqR = m_pinnaRight.ic1eq;
					var ic2eqR = m_pinnaRight.ic2eq;

					var a1R = m_processLocalSettings.pinnaRight.a1;
					var a2R = m_processLocalSettings.pinnaRight.a2;
					var a3R = m_processLocalSettings.pinnaLeft.a3;

					var m1R = m_processLocalSettings.pinnaRight.m1;

					// left & right svf bell dip filters
					for (int i = 0; i < length; i++)
					{
						float v3L = m_leftSource[i] - ic2eqL;
						float v1L = a1L * ic1eqL + a2L * v3L;
						float v2L = ic2eqL + a2L * ic1eqL + a3L * v3L;

						ic1eqL = 2f * v1L - ic1eqL;
						ic2eqL = 2f * v2L - ic2eqL;

						dest[i * 2] += m_leftSource[i] + m1L * v1L;

						float v3R = m_rightSource[i] - ic2eqR;
						float v1R = a1R * ic1eqR + a2R * v3R;
						float v2R = ic2eqR + a2R * ic1eqR + a3R * v3R;

						ic1eqR = 2f * v1R - ic1eqR;
						ic2eqR = 2f * v2R - ic2eqR;

						dest[i * 2 + 1] += m_rightSource[i] + m1R * v1R;
					}


					m_pinnaLeft.ic1eq = ic1eqL;
					m_pinnaLeft.ic2eq = ic2eqL;

					m_pinnaRight.ic1eq = ic1eqR;
					m_pinnaRight.ic2eq = ic2eqR;

				}
			}
		}

		public float[] m_leftSource, m_rightSource;
		private bool m_ready;
		SoundBase m_target;
		Surface m_possibleSurface;
		SoundEmitter m_possibleEmitter;
		PreMainPostProcesser m_processor;
		FractionalDelay.Tap m_mainTapLeft, m_mainTapRight;
		FractionalDelay.Tap m_pinnaTapLeft, m_pinnaTapRight;
		FilterSettings settings, m_processLocalSettings;
		StateVariableFilter m_pinnaLeft, m_pinnaRight;
		OnePole m_backLeft, m_backRight, m_posElevationLeft, m_posElevationRight;
		float m_oldGainLeft, m_oldGainRight;
	}
}
