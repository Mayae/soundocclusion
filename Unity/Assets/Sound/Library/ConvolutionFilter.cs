﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System;
using UnityEngine;


namespace Sound
{
	class ConvolutionFilter
	{

		public ConvolutionFilter(AudioClip impulseReverbSource, int blockSize)
		{
			if (impulseReverbSource.channels != 1)
				throw new ArgumentException("Impulse response must be mono!");

			M = impulseReverbSource.samples;
			L = blockSize;
			N = Mathf.NextPowerOfTwo(M + L);

			input = new Signal(N);
			filter = new Signal(N);

			var impulse = new float[M];
			impulseReverbSource.GetData(impulse, 0);

			Buffer.BlockCopy(impulse, 0, filter.real, 0, M);

			fft = new FFT2();
		}

		Signal input, filter;
		FFT2 fft;
		int N, M, L;
	}
}
