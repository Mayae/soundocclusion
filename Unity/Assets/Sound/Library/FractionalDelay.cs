﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System;
using UnityEngine;

namespace Sound
{
	public class FractionalDelay
	{
		public float SampleRate { get { return m_sampleRate; } }
		public float BufferSize { get { return m_systemBufSize; } }
		public float Gain { get; set; }


		public FractionalDelay(float sampleRate, int systemBufSize)
		{
			Gain = 1.0f;
			m_sampleRate = sampleRate;
			m_systemBufSize = systemBufSize;
			//EnsureSampleStorage(systemBufSize);
		}

		public struct Tap
		{
			public static bool s_DisableDelays;

			public Tap(FractionalDelay parent)
			{
				m_parent = parent;
				m_delay = 0;
				m_speed = 1.0f;
			}

			public void SetTarget(FractionalDelay target)
			{
				m_parent = target;
			}

			public void SetDelay(float delayInSeconds)
			{
				m_speed = 1.0f + (m_delay - delayInSeconds) / (m_parent.m_systemBufSize / m_parent.m_sampleRate);
				m_delay = delayInSeconds;
				m_parent.EnsureDelayCapacity(delayInSeconds);
			}

			public void SetDelayFromDistance(float meters)
			{
				SetDelay(DistanceToDelay(meters));
			}

			static public float DistanceToDelay(float meters)
			{
				if (s_DisableDelays)
					return 0;
				return (float)(meters / SoundProperties.Instance().SpeedOfSound);
			}

			public void DummyRead()
			{
				m_speed = 1.0f;
			}

			public void Read(float[] data)
			{
				// set speed to difference over delta/t
				if (!s_DisableDelays)
				{
					m_parent.Read(data, m_delay, m_speed);
				}
				else
				{
					m_parent.Read(data, 0, 1);
				}

				DummyRead();
			}

			public void ReadAdditively(float[] data, float strength)
			{
				// set speed to difference over delta/t
				if (!s_DisableDelays)
				{
					m_parent.ReadAdditively(data, m_delay, m_speed, strength);
				}
				else
				{
					m_parent.ReadAdditively(data, 0, 1, strength);
				}
				DummyRead();
			}

			private float m_speed;
			private float m_delay;
			private FractionalDelay m_parent;
		}

		public void AdvanceCursor(int size)
		{
			m_cursor += size;
			m_cursor &= (m_actualSize - 1);
			// increase current storage if less than our actual size
			m_curSize = Mathf.Min(m_actualSize, m_curSize + size);
			// clear next part of buffer

			if (m_buffer == null)
				return;

			var remainingSamples = size;
			var index = 0;
			var localCursor = m_cursor;
			while (remainingSamples > 0)
			{
				var availableSamples = m_actualSize - localCursor;
				var part = Utility.PartionedSaturatedSubtract(remainingSamples, availableSamples);

				Array.Clear(m_buffer, localCursor, part);

				localCursor += part;
				remainingSamples -= part;
				index += part;
				localCursor &= (m_actualSize - 1);
			}

		}

		public void Insert(float[] data)
		{
			unchecked
			{
				EnsureSampleStorage(data.Length);

				var remainingSamples = data.Length;
				var index = 0;
				var localCursor = m_cursor;
				while (remainingSamples > 0)
				{
					var availableSamples = m_actualSize - localCursor;
					var part = Utility.PartionedSaturatedSubtract(remainingSamples, availableSamples);

					Buffer.BlockCopy(data, index * sizeof(float), m_buffer, localCursor * sizeof(float), part * sizeof(float));

					localCursor += part;
					remainingSamples -= part;
					index += part;
					localCursor &= (m_actualSize - 1);
				}

			}
		}

		public void InsertAdditive(float[] data, float volume = 1.0f)
		{
			unchecked
			{
				EnsureSampleStorage(data.Length);

				var remainingSamples = data.Length;
				var index = 0;
				var localCursor = m_cursor;

				while (remainingSamples > 0)
				{
					var availableSamples = m_actualSize - localCursor;
					var part = Utility.PartionedSaturatedSubtract(remainingSamples, availableSamples);

					for(int i = 0; i < part; ++i)
					{
						m_buffer[localCursor + i] += data[index + i] * volume;
					}

					localCursor += part;
					remainingSamples -= part;
					index += part;
					localCursor &= (m_actualSize - 1);
				}

			}
		}

		public void Insert(ArraySegment<float> data, int sampleTimeOffset = 0)
		{
			unchecked
			{
				EnsureSampleStorage(data.Count);

				var remainingSamples = data.Count;
				var index = 0;
				var localCursor = m_cursor + sampleTimeOffset;
				localCursor &= (m_actualSize - 1);
				while (remainingSamples > 0)
				{
					var availableSamples = m_actualSize - localCursor;
					var part = Utility.PartionedSaturatedSubtract(remainingSamples + sampleTimeOffset, availableSamples);

					Buffer.BlockCopy(data.Array, data.Offset * sizeof(float) + index * sizeof(float), m_buffer, localCursor * sizeof(float), part * sizeof(float));

					localCursor += part;
					remainingSamples -= part;
					index += part;
					localCursor &= (m_actualSize - 1);
				}
			}
		}

		/// <summary>
		/// HACK! SHOULD BE PRIVATE
		/// </summary>
		/// <param name="delay"></param>
		public void EnsureDelayCapacity(float delay)
		{
			EnsureSampleStorage(m_systemBufSize * 2 + Mathf.CeilToInt(delay * m_sampleRate));
		}

		private void EnsureSampleStorage(int samples)
		{
			unchecked
			{
				if (samples > m_actualSize)
				{
					var newBuf = new float[Mathf.NextPowerOfTwo(samples)];
					var remainingSamples = m_curSize;
					var index = 0;

					while (remainingSamples > 0)
					{
						m_cursor &= (m_actualSize - 1);
						var availableSamples = m_actualSize - m_cursor;
						var part = Utility.PartionedSaturatedSubtract(remainingSamples, availableSamples);

						Buffer.BlockCopy(m_buffer, m_cursor * sizeof(float), newBuf, index * sizeof(float), part * sizeof(float));
						remainingSamples -= part;
						m_cursor += part;
						index += part;

					}
					m_buffer = newBuf;
					m_cursor = m_curSize;
					m_actualSize = m_buffer.Length;

				}
			}

			/*else
			{
			 ???
				throw new NotImplementedException("Has to shuffle buffer around if size is increased!");
			} */

		}
		/// <summary>
		/// HACK! SHOULD BE PRIVATE
		/// </summary>
		/// <param name="delay"></param>
		public void Read(float[] data, float delay)
		{
			Read(data, delay, 1.0f);
		}

		/// <summary>
		/// HACK! SHOULD BE PRIVATE
		/// Also delete this method
		/// </summary>
		/// <param name="delay"></param>
		public void ReadGained(float[] data, float delay, float gain)
		{

			if (m_curSize <= 0)
				return;
			ReadInternalWithGain(data, delay, 1.0f, gain);
		}

		public void Read(float[] data, float delay, float speed)
		{

			if (m_curSize <= 0)
				return;

			if (Gain != 1.0f)
				ReadInternalWithGain(data, delay, speed, Gain);
			else
				ReadInternalNormalized(data, delay, speed);
		}

		public void ReadAdditively(float[] data, float delay, float speed, float strength)
		{
			if (m_curSize <= 0)
				return;

			var volume = strength * Gain;

			if (volume != 1.0f)
				ReadInternalWithGainAdditive(data, delay, speed, volume);
			else
				ReadInternalNormalizedAdditive(data, delay, speed);
		}

		private void ReadInternalWithGain(float[] data, float delay, float speed, float gainToApply)
		{
			speed = Mathf.Max(speed, 0.0001f);
			if (m_actualSize == 0)
				return;

			unchecked
			{
				var delaySamples = delay * m_sampleRate;
				var fractionalSamples = Mathf.Min(m_curSize, data.Length * speed);
				var factor = fractionalSamples / data.Length;
				var x1 = m_cursor - fractionalSamples - delaySamples;
				var remainingSamples = data.Length;

				while (x1 < 0)
					x1 += m_curSize;


				var mask = (m_curSize - 1);

				for (int i = 0; i < data.Length; ++i)
				{
					if (x1 >= m_curSize)
						x1 -= m_curSize;
					int xfloor = (int)x1;
					var fraction = x1 - xfloor;
					// slightly faster but gives errors in growing transitions (m_cursize may not be 2^x where x = integer)
					//int x2 = (xfloor + 1) & mask;
					int x2 = (xfloor + 1);
					if (x2 >= m_curSize)
						x2 -= m_curSize;
					data[i] = gainToApply * (m_buffer[xfloor] * (1f - fraction) + m_buffer[x2] * fraction);

					x1 += factor;
				}
			}
		}

		private void ReadInternalWithGainAdditive(float[] data, float delay, float speed, float gainToApply)
		{
			speed = Mathf.Max(speed, 0.0001f);
			if (m_actualSize == 0)
				return;

			unchecked
			{
				var delaySamples = delay * m_sampleRate;
				var fractionalSamples = Mathf.Min(m_curSize, data.Length * speed);
				var factor = fractionalSamples / data.Length;
				var x1 = m_cursor - fractionalSamples - delaySamples;
				var remainingSamples = data.Length;

				while (x1 < 0)
					x1 += m_curSize;


				var mask = (m_curSize - 1);

				for (int i = 0; i < data.Length; ++i)
				{
					if (x1 >= m_curSize)
						x1 -= m_curSize;
					int xfloor = (int)x1;
					var fraction = x1 - xfloor;
					// slightly faster but gives errors in growing transitions (m_cursize may not be 2^x where x = integer)
					//int x2 = (xfloor + 1) & mask;
					int x2 = (xfloor + 1);
					if (x2 >= m_curSize)
						x2 -= m_curSize;
					data[i] += gainToApply * (m_buffer[xfloor] * (1f - fraction) + m_buffer[x2] * fraction);

					x1 += factor;
				}
			}
		}

		private void ReadInternalNormalizedAdditive(float[] data, float delay, float speed)
		{
			speed = Mathf.Max(speed, 0.0001f);
			if (m_actualSize == 0)
				return;

			unchecked
			{
				var delaySamples = delay * m_sampleRate;
				var fractionalSamples = Mathf.Min(m_curSize, data.Length * speed);
				var factor = fractionalSamples / data.Length;
				var x1 = m_cursor - fractionalSamples - delaySamples;
				var remainingSamples = data.Length;

				while (x1 < 0)
					x1 += m_curSize;


				var mask = (m_curSize - 1);

				for (int i = 0; i < data.Length; ++i)
				{
					if (x1 >= m_curSize)
						x1 -= m_curSize;
					int xfloor = (int)x1;
					var fraction = x1 - xfloor;
					// slightly faster but gives errors in growing transitions (m_cursize may not be 2^x where x = integer)
					//int x2 = (xfloor + 1) & mask;
					int x2 = (xfloor + 1);
					if (x2 >= m_curSize)
						x2 -= m_curSize;
					data[i] += m_buffer[xfloor] * (1f - fraction) + m_buffer[x2] * fraction;

					x1 += factor;
				}
			}
		}

		private void ReadInternalNormalized(float[] data, float delay, float speed)
		{
			speed = Mathf.Max(speed, 0.0001f);
			if (m_actualSize == 0)
				return;

			unchecked
			{
				var delaySamples = delay * m_sampleRate;
				var fractionalSamples = Mathf.Min(m_curSize, data.Length * speed);
				var factor = fractionalSamples / data.Length;
				var x1 = m_cursor - fractionalSamples - delaySamples;

				while (x1 < 0)
					x1 += m_curSize;

				var mask = (m_curSize - 1);

				for (int i = 0; i < data.Length; ++i)
				{
					if (x1 >= m_curSize)
						x1 -= m_curSize;
					int xfloor = (int)x1;
					var fraction = x1 - xfloor;
					int x2 = (xfloor + 1);
					if (x2 >= m_curSize)
						x2 -= m_curSize;
					data[i] = m_buffer[xfloor] * (1f - fraction) + m_buffer[x2] * fraction;

					x1 += factor;
				}
			}
		}

		float m_sampleRate;
		float[] m_buffer;
		int m_systemBufSize;
		int m_readOffset;
		int m_curSize;
		int m_actualSize;
		int m_cursor;
		int m_capacity;
	}
}
