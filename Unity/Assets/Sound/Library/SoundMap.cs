﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sound.Behaviours;
using UnityEngine;

namespace Sound
{
	public class SoundMap
	{
		public class TreeNode
		{
			public TreeNode(Joint payloadContent)
			{
				payload = payloadContent;
				branches = new List<TreeNode>();
			}

			public float distanceDONOTUSE;
			public Joint payload;
			public List<TreeNode> branches;
		}

		public struct Joint
		{
			public Surface connection;
			public Surface.Face entryFace;
			public float distance;
			public float reflectionAngleAttenuation;
		}

		private struct LinkWeight
		{
			public Surface connection;
			public float distance;
			public float penalty;
			public float angle;
		}

		public TreeNode TraceSoundMap(Vector3 soundSourceOrigin)
		{
			TreeNode root = new TreeNode(new Joint { connection = null, reflectionAngleAttenuation = 1.0f });

			int layerMask = (1 << 2) | (1 << 8);

			var pos = soundSourceOrigin;
			var totalVisibleSurfaces = SoundEngine.Instance.GetSurfaces().Where(
				s =>
				{
					var destination = s.Position;


					if(!s.receivesSoundTransparently)
					{
						var difference = destination - pos;
						var direction = (difference).normalized;
						var distance = difference.magnitude;

						Ray r = new Ray(pos, direction);
						RaycastHit hit;

						if (!Physics.Raycast(r, out hit, distance))
							return false;
						if (hit.collider.gameObject == s.gameObject)
							return true;
					}
					else
					{
						return Physics.Linecast(pos, destination, layerMask);
					}

					return false;
				}
			).ToList();

			if (totalVisibleSurfaces.Count == 0)
				return root;

			var nonVisible = SoundEngine.Instance.GetSurfaces().Except(totalVisibleSurfaces).ToList();

			var paths = new List<List<Joint>>();

			// this step sorts the list backwards, so we start by tracing to objects
			// furthest away, thus eliminating most stuff in between through larger paths
			Utility.SortListByDistance(nonVisible, soundSourceOrigin, false);

			while (nonVisible.Count > 0)
			{
				// select the most distance object 
				var destination = nonVisible[0];

				// select the most appropriate starting surface ("target") (the one closest to the line between us and the destination)
				if (totalVisibleSurfaces.Count > 1)
					Utility.SortListByDistance(totalVisibleSurfaces, pos, destination.Position);

				foreach(var target in totalVisibleSurfaces)
				{
					var path = new List<Joint>();

					if (FindPathConnectingSurfaces(pos, destination, target, ref path))
					{
						// we don't trace sounds to any non-visible parts we already indirectly visited
						foreach (var possibleNonVisibleCandidate in path)
							nonVisible.Remove(possibleNonVisibleCandidate.connection);

						// store the possible path
						paths.Add(path);
						break;
					}
				}



				// remove this destination and move on
				nonVisible.Remove(destination);

			}

			foreach (var target in totalVisibleSurfaces)
			{
				float angle;
				var face = target.FaceFromPoint(pos, out angle);

				var distance = target.Position - pos;

				root.branches.Add(new TreeNode(
					new Joint {
						connection = target,
						reflectionAngleAttenuation = Mathf.Abs(Mathf.Cos(angle)),
						distance = distance.magnitude,
						entryFace = face
					})
				);
			}

			JoinPathsToTree(root, paths);

			return root;
		}

		public void JoinPathsToTree(TreeNode root, List<List<Joint>> lists)
		{
			// note this implementation is not entirely correct,
			// branches should be matched on a second pass (multiple entries in the tree can occur right now)
			foreach (var path in lists)
			{
				if (path.Count == 0)
					continue;

				TreeNode current = root;

				for (int i = 0; i < path.Count; ++i)
				{
					bool success = false;

					for (int b = 0; b < current.branches.Count; ++b)
					{
						if (current.branches[b].payload.connection == path[i].connection && current.branches[b].payload.entryFace == path[i].entryFace)
						{
							//if(current.branches[b].branches.Count != 0
							//							)

							current = current.branches[b];
							success = true;
							break;
						}
					}

					if (!success)
					{
						var node = new TreeNode(path[i]);
						current.branches.Add(node);
						current = node;

					}
				}

			}

		}

		public bool FindPathConnectingSurfaces(Vector3 lookPoint, Surface destination, Surface connection, ref List<Joint> path)
		{
			var possibleObjects = SoundEngine.Instance.GetSurfaces().ToList();

			possibleObjects.Remove(destination);
			possibleObjects.Remove(connection);
			float angle;
			var face = connection.FaceFromPoint(lookPoint, out angle);

			path.Add(new Joint
			{
				connection = connection,
				reflectionAngleAttenuation = Mathf.Abs(Mathf.Cos(angle)),
				distance = (connection.Position - lookPoint).magnitude,
				entryFace = face,
			});

			if (FindPathToObject(lookPoint, destination, connection, possibleObjects, ref path))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// </summary>
		/// <param name="destination"></param>
		/// <param name="origin"></param>
		/// <param name="possibleObjects">
		/// Mustn't include destination nor origin</param>
		/// <param name="path"></param>
		/// <returns></returns>
		public bool FindPathToObject(Vector3 lookPoint, Surface destination, Surface origin, List<Surface> possibleObjects, ref List<Joint> path)
		{
			// initial visibility?

			var surfaceMap = SoundEngine.Instance.GetSurfaceMap();

			var destinationPosition = destination.Position;
			var originPosition = origin.Position;

			LinkWeight translucentCandidate = new LinkWeight { connection = null, penalty = float.MaxValue };


			if (surfaceMap[destination][origin].lineOfSight == SoundEngine.Visibility.HitType.Hit)
			{
				var normal = origin.Normal;


				var entryDifference = originPosition - lookPoint;
				var exitDifference = originPosition - destinationPosition;

				var exitDifferenceLength = exitDifference.magnitude;


				var inAngle = Vector3.Angle(entryDifference, normal);
				var outAngle = Vector3.Angle(exitDifference, normal);

				// angle in degrees
				var hemisIn = inAngle > 90;
				var hemisOut = outAngle > 90;

				var inputFace = destination.FaceFromPoint(originPosition);


				if (hemisIn == hemisOut)
				{
					path.Add(new Joint
					{
						connection = destination,
						reflectionAngleAttenuation = Mathf.Abs(Mathf.Cos(inAngle)),
						distance = exitDifferenceLength,
						entryFace = inputFace
					});
					return true;
				}
				else
				{
					var linkDistance = entryDifference.magnitude + exitDifferenceLength;
					var penalty = (1.0f / (linkDistance * linkDistance)) / (1.0f - origin.Reflectivity);

					if (possibleObjects.Count == 0 && penalty < float.MaxValue)
					{
						path.Add(new Joint
						{
							connection = destination,
							reflectionAngleAttenuation = Mathf.Abs(Mathf.Cos(inAngle)),
							distance = exitDifferenceLength,
							entryFace = inputFace
						});

						return true;
					}


					translucentCandidate.penalty = penalty;
					translucentCandidate.connection = destination;
					// TODO: invert?
					translucentCandidate.angle = (inAngle + outAngle) - 90;
					translucentCandidate.distance = exitDifferenceLength;
				}
			}

			if (possibleObjects.Count == 0)
				return false;

			Utility.SortListByDistance(possibleObjects, originPosition, destinationPosition);



			foreach (var conn in possibleObjects)
			{
				var c = conn;
				if (surfaceMap[origin][c].lineOfSight == SoundEngine.Visibility.HitType.Hit)
				{
					bool swapFaces = false;
					var normal = origin.Normal;

					var entryDifference = originPosition - lookPoint;
					var exitDifference = originPosition - c.Position;

					var exitDifferenceLength = exitDifference.magnitude;

					var inAngle = Vector3.Angle(entryDifference, normal);
					var outAngle = Vector3.Angle(exitDifference, normal);

					// angle in degrees
					var hemisIn = inAngle > 90;
					var hemisOut = outAngle > 90;

					var accAngle = inAngle + outAngle;

					if (hemisIn != hemisOut)
					{
						var linkDistance = entryDifference.magnitude + exitDifference.magnitude;
						var penalty = (1.0f / (linkDistance * linkDistance)) / (1.0f - c.Reflectivity);

						if (translucentCandidate.penalty > penalty)
						{
							translucentCandidate.penalty = penalty;
							translucentCandidate.connection = c;
							translucentCandidate.angle = inAngle * Mathf.Deg2Rad;
							translucentCandidate.distance = exitDifferenceLength;
						}
						continue;
					}
					else if(translucentCandidate.connection != null)
					{
						var linkDistance = 1.0f / (entryDifference.magnitude + exitDifferenceLength);

						// swap connections as occlusion is a better fit
						if (translucentCandidate.penalty < linkDistance)
						{
							c = translucentCandidate.connection;
							accAngle = translucentCandidate.angle;
							exitDifferenceLength = translucentCandidate.distance;
							swapFaces = true;
						}
	
					}

					var entryFace = c.FaceFromPoint(originPosition);

					var newList = possibleObjects.ToList();
					var newPath = path.ToList();
					newList.Remove(origin);
					newList.Remove(c);
					newPath.Add(new Joint {
						connection = c,
						reflectionAngleAttenuation = Mathf.Abs(Mathf.Cos(inAngle * Mathf.Deg2Rad)),
						distance = exitDifferenceLength,
						entryFace = entryFace
					});

					if (destination == c || FindPathToObject(originPosition, destination, c, newList, ref newPath))
					{
						path = newPath;
						return true;
					}
				}
			}

			// special case. sorry lazy
			if(translucentCandidate.connection != null && translucentCandidate.penalty < float.MaxValue)
			{
				var c = translucentCandidate.connection;
				possibleObjects.Remove(c);
				var newPath = path.ToList();

				var entryFace = c.FaceFromPoint(originPosition);

				newPath.Add(new Joint
				{
					connection = c,
					reflectionAngleAttenuation = 0.5f * (1 - Mathf.Cos(Mathf.Deg2Rad * (translucentCandidate.angle))),
					distance = translucentCandidate.distance,
					entryFace = entryFace
				});

				if (FindPathToObject(originPosition, destination, c, possibleObjects, ref newPath))
				{
					path = newPath;
					return true;
				}
			}

			return false;
		}



	}
}
