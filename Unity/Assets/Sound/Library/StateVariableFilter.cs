﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System;

namespace Sound
{
	public struct StateVariableFilter
	{
		[System.Serializable]
		public struct PeakFilterDescription
		{
			public float centerFrequency;
			public float quality;
			public float gainInDbs;
		}

		public struct DoubleCoefficients
		{
			public double A, g, k, a1, a2, a3, m0, m1, m2;
		}

		public struct Coefficients
		{
			public float A, g, k, a1, a2, a3, m0, m1, m2;
		}

		static public Coefficients DesignBell(float fc, float quality, float linearGain)
		{

			float A = linearGain;
			float g = Mathf.Tan(Mathf.PI * fc);
			float k = 1 / (quality * A);
			float a1 = 1 / (1 + g * (g + k));
			float a2 = g * a1;
			float a3 = g * a2;
			float m0 = 1;
			float m1 = k * (A * A - 1);
			float m2 = 0;
			return new Coefficients { A = A, g = g, k = k, a1 = a1, a2 = a2, a3 = a3, m0 = m0, m1 = m1, m2 = m2 };
		}

		static public DoubleCoefficients DesignBellDouble(double fc, double quality, double linearGain)
		{
			double A = linearGain;
			double g = Math.Tan(Math.PI * fc);
			double k = 1 / (quality * A);
			double a1 = 1 / (1 + g * (g + k));
			double a2 = g * a1;
			double a3 = g * a2;
			double m0 = 1;
			double m1 = k * (A * A - 1);
			double m2 = 0;
			return new DoubleCoefficients { A = A, g = g, k = k, a1 = a1, a2 = a2, a3 = a3, m0 = m0, m1 = m1, m2 = m2 };
		}

		public float ic1eq, ic2eq;
	}
}
