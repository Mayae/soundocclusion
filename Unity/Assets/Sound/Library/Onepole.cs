﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;


namespace Sound
{
	public struct OnePole
	{
		public enum Type
		{
			LP,
			HP
		}

		public struct Coefficients
		{
			public float a0, b1;

			static public Coefficients Identity { get { return new Coefficients { a0 = 1, b1 = 0 }; } }
		}

		public static Coefficients Design(Type type, float cutoff, float sampleRate)
		{
			float fc = cutoff / sampleRate;
			float a0, b1;

			switch(type)
			{
				case Type.HP:
					b1 = -Mathf.Exp(-2.0f * Mathf.PI * (0.5f - fc));
					a0 = 1.0f + b1;
					break;

				default:
					// an okay approximation is:
					// fc -= 1
					// b1 = fc * fc * fc * fc * fc * fc
					b1 = Mathf.Exp(-2.0f * Mathf.PI * fc);
					a0 = 1.0f - b1;
					break;
			}
			return new Coefficients { a0 = a0, b1 = b1 };
		}

		public float z1;
	}
}
