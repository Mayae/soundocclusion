﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class SceneDescription : MonoBehaviour {
	[TextArea(3, 10)]
	public string text;
	public float size = 0.3f;

	Rect rect;
	string title;
	void OnGUI()
	{

		rect = GUI.Window(
			0, 
			rect, 
			a => 
			{
				var area = rect;
				area.position = new Vector2(2, 17);
				area.size = new Vector2(area.width - 4, area.height - 20);
				GUI.TextArea(area, text);
			}, 
			title
		);

	}

	// Use this for initialization
	void Start () {
		title = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

		rect = new Rect(Screen.width * (1 - size), Screen.height * (1 - size), Screen.width * size, Screen.height * size);

	}

	// Update is called once per frame
	void Update () {
	
	}
}
