﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Linq;
using System.Collections;

public class AudioFileSwapper : MonoBehaviour
{
	public AudioClip[] clips;
	public UnityEngine.UI.Dropdown selector;

	private void OnClipSelected(int index)
	{
		InstallClip(clips[index]);
	}

	void Start()
	{
		selector.AddOptions(clips.Select(ac => ac.name).ToList());

		AudioClip startingClip = null;

		foreach (var psource in GameObject.FindObjectsOfType<Sound.ClipPointSource>())
		{
			startingClip = psource.clip;
			break;
		}

		if(startingClip != null)
		{
			for(int i = 0; i < clips.Length; ++i)
			{
				if(startingClip == clips[i])
				{
					selector.value = i;
					break;
				}
			}
		}

		selector.onValueChanged.AddListener(OnClipSelected);
	}

	public void InstallClip(AudioClip clip)
	{
		foreach(var psource in GameObject.FindObjectsOfType<Sound.ClipPointSource>())
		{
			psource.clip = clip;
			psource.ReloadClip();
		}
	}

}
