﻿/*************************************************************************************

	SoundOcclusion - Prototype implementation - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class DoorPusher : MonoBehaviour
{
	public GameObject objectToInteract;
	public float timeBeforeAction = 4;
	bool hasBeenExecuted;


	// Update is called once per frame
	void Update ()
	{
		if(!hasBeenExecuted && Time.time > timeBeforeAction)
		{
			hasBeenExecuted = true;
			var pos = objectToInteract.transform.localPosition;
			pos.x -= 2;
			objectToInteract.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.one * 20, pos);
		}
	}
}
