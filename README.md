Proof-of-concept repository of a real-time sound occlusion system complimentary to my thesis found here: http://www.jthorborg.com/index.html?ipage=occlusion

This repo is not supported nor updated nor an example of production code, it is only placed in public domain in the hope of being helpful to others.

See also this video: 
https://www.youtube.com/watch?v=cRWPs-Ns1Pc